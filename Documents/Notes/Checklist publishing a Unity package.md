# Decide which Unity version to support

- Latest LTS version
- Update local editor to correct minor version (patch versions can be skipped)

# Update project

- Open Package Manager in Unity Editor (Window -> Package Manager)
- Update Asset Store Tools
  - Remove old version
  - Packages: My Assets -> Asset Store Publishing Tools -> Update -> Import

# Test project

- Project compiles
- Unit Tests all successful 
  - Window -> General -> Test Runner
  - Run All
- Reimport all to check that no warnings are triggered
  - Assets -> Reimport All -> Reimport
  - Warnings for NGUI are okay

# Adjust project

- Add changelog for version in Readme.txt
  - Take subjects from commit logs since last version (tag)

# Create new version draft

- Go to https://publisher.unity.com/packages
- Create new draft for package
- Adjust version number
- Adjust version changes
- Remove old packages

# Build

## Build Addon packages

- Close Unity
- Run Tools/Build/BuildUnityPackageXX.bat (Unity version specific)
- Name: Slash.Unity.DataBind.unitypackage
- Make sure build runs through

## Asset Store Package Upload

- Open Unity
- Make sure Addon packages are still available (were generated before)
- Asset Store Tools -> Asset Store Uploader
- Use Upload Type "From Assets Folder" and "Assets/Slash.Unity.DataBind" as "Folder path" (no Dependencies)
- "Export and Upload" package
- Copy uploaded unitypackage from Temp into Tests

## Test uploaded package

- Create new project in Tests folder (Name: Version)
- Import DataBind.unitypackage in project
- Test Unity UI examples for functionality

- Import Addons/TextMeshPro.unitypackage

# Tag version

- Tag version in git with "version_number"
- Push repository (Include Tags)

# Release version

- Go to publisher site: https://publisher.unity.com/packages
- Make sure to update version number
- Add changelog if not already done
- Submit package

# Bitbucket

- https://bitbucket.org/coeing/data-bind/admin/issues/versions
- Add new version
- Add new milestone for next version

# Announce release

- In official Unity forum thread: https://forum.unity3d.com/threads/released-data-bind-for-unity.298471

Release Post:

```
The new version 1.xy of Data Bind was just submitted and is now in review :)

If there are any issues you are still missing, there is a public issue tracker where everybody can add their wishes, bug reports, ideas, feedback,...:

https://bitbucket.org/coeing/data-bind/issues

The main additions:
-

Here is the full changelog:
[SPOILER="Changelog"]
[/SPOILER]

The new version will be available in a few days in the Unity Asset Store:

https://assetstore.unity.com/packages/tools/gui/data-bind-for-unity-28301
```