# Data Bind for Unity

Repository for Unity Asset **Data Bind for Unity**

https://assetstore.unity.com/packages/tools/gui/data-bind-for-unity-28301

## License

If you are using Data Bind in a personal project or to evaluate its usage, feel free to use the source from this repository. 

If you use it in a professional project (or like to support its development :) ), please buy it in the Unity Asset Store.

## Bug reports and feature requests

If you find any bugs or have a wish for a new feature, please create an issue in the official issue tracker: https://bitbucket.org/coeing/data-bind/issues

## Support

For general questions please use the official Unity forum thread: https://forum.unity.com/threads/released-data-bind-for-unity.298471 I will check it regurlary and try to answer each question. But the main advantage is that other devs might be able to answer it quicker and will benefit from the answer.

## Pull Requests

If you encountered any use cases in your projects that made it necessary to add some functionality to the asset, feel free to create a pull request for it. I will happily check to see if I can integrate it in the main asset, so other devs will benefit from it as well.