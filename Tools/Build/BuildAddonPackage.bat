REM Setup
REM Set environment variables:
REM UNITY_PATH is the path to your Unity3D installation (e.g. C:\Program Files\Unity)

@echo off

set ADDON_PATH=%~1
set ADDON_PACKAGE_NAME=%~2

setlocal

set UNITY_PROJECT_PATH=%~dp0..\..\Source\DataBind.Unity
set UNITY_DATABIND_PATH=%UNITY_PROJECT_PATH%\Assets\Slash.Unity.DataBind
set UNITY_PACKAGE_PATH=%UNITY_DATABIND_PATH%\Addons\%ADDON_PACKAGE_NAME%.unitypackage

REM Remove old package
if exist "%UNITY_PACKAGE_PATH%" (
  del "%UNITY_PACKAGE_PATH%"
)

echo.
echo Build addon %ADDON_PACKAGE_NAME%

set ADDON_SOURCE_PATH=Assets/Slash.Unity.DataBind.Addons/%ADDON_PATH%
set ADDON_EXPORT_PATH=Assets/Slash.Unity.DataBind/Addons/%ADDON_PACKAGE_NAME%

echo Move sources to %ADDON_EXPORT_PATH%
	
robocopy /move /e /nfl /ndl /njh /njs /nc /ns /np "%UNITY_PROJECT_PATH%/%ADDON_SOURCE_PATH%" "%UNITY_PROJECT_PATH%/%ADDON_EXPORT_PATH%" >nul 2>&1
robocopy /move /nfl /ndl /njh /njs /nc /ns /np "%UNITY_PROJECT_PATH%/%ADDON_SOURCE_PATH%/.." "%UNITY_PROJECT_PATH%/%ADDON_EXPORT_PATH%/.." "%ADDON_PACKAGE_NAME%.meta" >nul 2>&1

REM Build package
"%UNITY_PATH%\Editor\Unity.exe" -batchmode -projectPath "%UNITY_PROJECT_PATH%" -exportPackage "%ADDON_EXPORT_PATH%" "%UNITY_PACKAGE_PATH%" -quit -logFile "%ADDON_PACKAGE_NAME%log.txt" > "log.txt"

echo Move sources back to %ADDON_SOURCE_PATH%
	
robocopy /move /e /nfl /ndl /njh /njs /nc /ns /np "%UNITY_PROJECT_PATH%/%ADDON_EXPORT_PATH%" "%UNITY_PROJECT_PATH%/%ADDON_SOURCE_PATH%" >nul 2>&1
robocopy /move /nfl /ndl /njh /njs /nc /ns /np "%UNITY_PROJECT_PATH%/%ADDON_EXPORT_PATH%/.." "%UNITY_PROJECT_PATH%/%ADDON_SOURCE_PATH%/.." "%ADDON_PACKAGE_NAME%.meta" >nul 2>&1

endlocal