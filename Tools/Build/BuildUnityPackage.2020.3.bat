@echo off

if not defined UNITY_2020_3_PATH (
    echo Environment variable UNITY_2020_3_PATH not defined
	pause
    exit
)

set UNITY_PATH=%UNITY_2020_3_PATH%
call BuildUnityPackage.bat

pause