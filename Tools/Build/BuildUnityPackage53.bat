@echo off

if not defined UNITY_5_3_PATH (
    echo Environment variable UNITY_5_3_PATH not defined
    exit
)

set UNITY_PATH=%UNITY_5_3_PATH%
call BuildUnityPackage.bat