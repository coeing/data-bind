@echo off

if not defined UNITY_2018_4_PATH (
    echo Environment variable UNITY_2018_4_PATH not defined
	pause
    exit
)

set UNITY_PATH=%UNITY_2018_4_PATH%
call BuildUnityPackage.bat

pause