REM Setup
REM Set environment variables:
REM UNITY_PATH is the path to your Unity3D installation (e.g. C:\Program Files\Unity)

@echo off

if not exist "%UNITY_PATH%" (
    echo Couldn't find Unity at path '%UNITY_PATH%'
    exit
)

set BATCH_DIR=%~dp0
set UNITY_PROJECT_PATH=%~dp0..\..\Source\DataBind.Unity
set UNITY_DATABIND_PATH=%UNITY_PROJECT_PATH%\Assets\Slash.Unity.DataBind
set TMP_FOLDER=%~dp0TmpAddons
mkdir "%TMP_FOLDER%"

echo Using Unity installation at %UNITY_PATH%

REM Build addons
echo Building add ons from %UNITY_DATABIND_PATH%.Addons\*
for /d %%s in ("%UNITY_DATABIND_PATH%.Addons\*") do (
	call BuildAddonPackage.bat %%~nxs %%~nxs
)

REM Remove settings file
set DATA_BIND_SETTINGS_PATH=%UNITY_DATABIND_PATH%\Resources\Data Bind Settings.asset
if exist "%DATA_BIND_SETTINGS_PATH%" (
  del "%DATA_BIND_SETTINGS_PATH%"
)

REM Build complete package
set UNITY_PACKAGE_PATH=%~dp0Slash.Unity.DataBind.unitypackage

REM Remove old package
if exist "%UNITY_PACKAGE_PATH%" (
  del "%UNITY_PACKAGE_PATH%"
)

REM Build package
"%UNITY_PATH%\Editor\Unity.exe" -batchmode -projectPath "%UNITY_PROJECT_PATH%" -exportPackage "Assets/Slash.Unity.DataBind" "%UNITY_PACKAGE_PATH%" -quit > log.txt

REM Move addon sources back
robocopy /move /e /nfl /ndl /njh /njs /nc /ns /np "%TMP_FOLDER%" "%UNITY_PROJECT_PATH%\Assets"