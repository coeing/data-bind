﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SpriteNameSetter.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.UI.NGUI.Setters
{
    using Slash.Unity.DataBind.Foundation.Setters;
    using UnityEngine;

    /// <summary>
    ///     Set the sprite name of a UISprite depending on the string data value.
    /// </summary>
    [AddComponentMenu("Data Bind/NGUI/Setters/[DB] Sprite Name Setter (NGUI)")]
    public class SpriteNameSetter : ComponentSingleSetter<UISprite, string>
    {
        /// <inheritdoc />
        protected override void UpdateTargetValue(UISprite target, string value)
        {
            target.spriteName = value;
        }
    }
}