﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InputValueSetter.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.UI.NGUI.Setters
{
    using Slash.Unity.DataBind.Foundation.Setters;
    using UnityEngine;

    /// <summary>
    ///     Set the value of a UIInput depending on the string data value.
    /// </summary>
    [AddComponentMenu("Data Bind/NGUI/Setters/[DB] Input Value Setter (NGUI)")]
    public class InputValueSetter : ComponentSingleSetter<UIInput, string>
    {
        /// <inheritdoc />
        protected override void UpdateTargetValue(UIInput target, string value)
        {
            target.value = value;
        }
    }
}