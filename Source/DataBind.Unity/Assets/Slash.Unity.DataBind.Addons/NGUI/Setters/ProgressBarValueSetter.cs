﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProgressBarValueSetter.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.UI.NGUI.Setters
{
    using Slash.Unity.DataBind.Foundation.Setters;
    using UnityEngine;

    /// <summary>
    ///     Set the bar value of a UIProgressBar depending on the float data value.
    /// </summary>
    [AddComponentMenu("Data Bind/NGUI/Setters/[DB] Progress Bar Value Setter (NGUI)")]
    public class ProgressBarValueSetter : ComponentSingleSetter<UIProgressBar, float>
    {
        /// <inheritdoc />
        protected override void UpdateTargetValue(UIProgressBar target, float value)
        {
            target.value = value;
        }
    }
}