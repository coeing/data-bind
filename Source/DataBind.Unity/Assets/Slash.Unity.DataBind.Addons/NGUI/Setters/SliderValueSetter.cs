﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SliderValueSetter.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.UI.NGUI.Setters
{
    using Slash.Unity.DataBind.Foundation.Setters;
    using UnityEngine;

    /// <summary>
    ///     Sets the value of a slider depending on a data value.
    ///     <para>Input: Number</para>
    /// </summary>
    [AddComponentMenu("Data Bind/NGUI/Setters/[DB] Slider Value Setter (NGUI)")]
    public class SliderValueSetter : ComponentSingleSetter<UISlider, float>
    {
        /// <inheritdoc />
        protected override void UpdateTargetValue(UISlider target, float value)
        {
            target.value = value;
        }
    }
}