﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GridItemsSetter.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.UI.NGUI.Setters
{
    using System.Collections;

    using Slash.Unity.DataBind.Foundation.Setters;

    using UnityEngine;

    /// <summary>
    ///   Set the items of a UIGrid depending on the items of the collection data value.
    /// </summary>
    [AddComponentMenu("Data Bind/NGUI/Setters/[DB] Grid Items Setter (NGUI)")]
    public sealed class GridItemsSetter : GameObjectItemsSetter
    {
        private UIGrid uiGrid;

        /// <summary>
        ///   UI Grid to work on.
        /// </summary>
        public UIGrid UIGrid
        {
            get
            {
                return this.uiGrid ?? (this.uiGrid = this.Target != null ? this.Target.GetComponent<UIGrid>() : null);
            }
        }

        /// <summary>
        ///   Called when the items of the control changed.
        /// </summary>
        protected override void OnItemsChanged()
        {
            // Reposition grid.
            this.StartCoroutine(this.RepositionGrid());
        }

        private IEnumerator RepositionGrid()
        {
            // Reposition now.
            this.UIGrid.Reposition();

            // Wait one frame and reposition again, items may not be created yet.
            yield return new WaitForSeconds(0);

            this.UIGrid.Reposition();
        }
    }
}