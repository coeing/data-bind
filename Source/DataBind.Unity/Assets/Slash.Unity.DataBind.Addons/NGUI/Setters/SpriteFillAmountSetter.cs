﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SpriteFillAmountSetter.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.UI.NGUI.Setters
{
    using Slash.Unity.DataBind.Foundation.Setters;
    using UnityEngine;

    /// <summary>
    ///     Set the fill amount of a UISprite depending on the float data value.
    /// </summary>
    [AddComponentMenu("Data Bind/NGUI/Setters/[DB] Sprite Fill Amount Setter (NGUI)")]
    public class SpriteFillAmountSetter : ComponentSingleSetter<UISprite, float>
    {
        /// <inheritdoc />
        protected override void UpdateTargetValue(UISprite target, float value)
        {
            target.fillAmount = value;
        }
    }
}