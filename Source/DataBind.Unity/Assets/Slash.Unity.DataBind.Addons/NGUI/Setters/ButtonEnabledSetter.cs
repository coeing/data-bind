﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ButtonEnabledSetter.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.UI.NGUI.Setters
{
    using Slash.Unity.DataBind.Foundation.Setters;
    using UnityEngine;

    /// <summary>
    ///     Sets the enabled flag of a button depending on a data value.
    ///     <para>Input: Boolean</para>
    /// </summary>
    [AddComponentMenu("Data Bind/NGUI/Setters/[DB] Button Enabled Setter (NGUI)")]
    public class ButtonEnabledSetter : ComponentSingleSetter<UIButton, bool>
    {
        /// <inheritdoc />
        protected override void UpdateTargetValue(UIButton target, bool value)
        {
            target.isEnabled = value;
        }
    }
}