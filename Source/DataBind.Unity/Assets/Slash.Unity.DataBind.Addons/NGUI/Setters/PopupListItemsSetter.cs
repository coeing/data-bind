﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PopupListItemsSetter.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.UI.NGUI.Setters
{
    using Slash.Unity.DataBind.Foundation.Setters;

    using UnityEngine;

    /// <summary>
    ///   Sets the items of a UIPopupList depending on a string data list.
    /// </summary>
    [AddComponentMenu("Data Bind/NGUI/Setters/[DB] Popup List Items Setter (NGUI)")]
    public class PopupListItemsSetter : ItemsSetter
    {
        private UIPopupList uiPopupList;

        /// <summary>
        ///   Popup list to work on.
        /// </summary>
        public UIPopupList UIPopupList
        {
            get
            {
                return this.uiPopupList
                       ?? (this.uiPopupList = this.Target != null ? this.Target.GetComponent<UIPopupList>() : null);
            }
        }

        /// <inheritdoc />
        protected override void ClearItems()
        {
            if (this.UIPopupList != null)
            {
                this.UIPopupList.Clear();
            }
        }

        /// <inheritdoc />
        protected override void CreateItem(object itemContext, int itemIndex)
        {
            if (this.UIPopupList == null)
            {
                return;
            }

            var itemValue = this.CreateItemValue(itemContext);
            this.UIPopupList.AddItem(itemValue, itemContext);
        }

        /// <inheritdoc />
        protected override void RemoveItem(object itemContext)
        {
            if (this.UIPopupList == null)
            {
                return;
            }

            var itemValue = this.CreateItemValue(itemContext);
            this.UIPopupList.items.Remove(itemValue);
        }

        private string CreateItemValue(object itemContext)
        {
            // TODO(co): This should be more generic, e.g. if the item is a context, a data property could be used as the value.
            return itemContext != null ? itemContext.ToString() : string.Empty;
        }
    }
}