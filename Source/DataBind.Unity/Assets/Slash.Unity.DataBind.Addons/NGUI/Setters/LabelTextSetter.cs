﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LabelTextSetter.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.UI.NGUI.Setters
{
    using Slash.Unity.DataBind.Foundation.Setters;
    using UnityEngine;

    /// <summary>
    ///     Set the text of a UILabel depending on the string data value.
    /// </summary>
    [AddComponentMenu("Data Bind/NGUI/Setters/[DB] Label Text Setter (NGUI)")]
    public class LabelTextSetter : ComponentSingleSetter<UILabel, string>
    {
        /// <inheritdoc />
        protected override void UpdateTargetValue(UILabel target, string value)
        {
            target.text = value;
        }
    }
}