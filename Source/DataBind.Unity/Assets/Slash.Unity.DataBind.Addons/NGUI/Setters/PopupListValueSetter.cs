﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PopupListValueSetter.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.UI.NGUI.Setters
{
    using Slash.Unity.DataBind.Foundation.Setters;
    using UnityEngine;

    /// <summary>
    ///     Sets the value of a popup list depending on the string data value.
    /// </summary>
    [AddComponentMenu("Data Bind/NGUI/Setters/[DB] Popup List Value Setter (NGUI)")]
    public class PopupListValueSetter : ComponentSingleSetter<UIPopupList, string>
    {
        /// <inheritdoc />
        protected override void UpdateTargetValue(UIPopupList target, string value)
        {
            // Checking if value changed as the NGUI popup list doesn't do it internally.
            // ReSharper disable once RedundantCheckBeforeAssignment
            if (value != target.value)
            {
                target.value = value;
            }
        }
    }
}