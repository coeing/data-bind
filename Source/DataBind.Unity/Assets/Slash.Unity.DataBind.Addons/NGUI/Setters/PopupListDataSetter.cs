﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PopupListDataSetter.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.UI.NGUI.Setters
{
    using Slash.Unity.DataBind.Foundation.Setters;
    using UnityEngine;

    /// <summary>
    ///     Sets the value of a popup list depending on the data value.
    /// </summary>
    [AddComponentMenu("Data Bind/NGUI/Setters/[DB] Popup List Data Value Setter (NGUI)")]
    public class PopupListDataSetter : ComponentSingleSetter<UIPopupList, object>
    {
        /// <inheritdoc />
        protected override void UpdateTargetValue(UIPopupList target, object value)
        {
            // Get text value for data value.
            var dataValueIndex = target.itemData.IndexOf(value);
            string textValue = null;
            if (dataValueIndex >= 0)
            {
                textValue = target.items[dataValueIndex];
            }

            // Checking if value changed as the NGUI popup list doesn't do it internally.
            // ReSharper disable once RedundantCheckBeforeAssignment
            if (textValue != target.value)
            {
                target.value = textValue;
            }
        }
    }
}