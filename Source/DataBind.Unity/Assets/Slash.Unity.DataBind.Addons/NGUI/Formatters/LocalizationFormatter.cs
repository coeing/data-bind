﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LocalizationFormatter.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.UI.NGUI.Formatters
{
    using Slash.Unity.DataBind.Core.Presentation;
    using UnityEngine;

    /// <summary>
    ///     Uses the localization system of NGUI to convert a localization key into a localized string.
    /// </summary>
    [AddComponentMenu("Data Bind/NGUI/Formatters/[DB] Localization Formatter (NGUI)")]
    public class LocalizationFormatter : DataProvider
    {
        /// <summary>
        ///     Data to bind to.
        /// </summary>
        public DataBinding Data;

        /// <inheritdoc />
        public override object Value
        {
            get
            {
                var key = this.Data.GetValue<string>();
                return !string.IsNullOrEmpty(key) ? Localization.Get(key) : null;
            }
        }

        /// <inheritdoc />
        public override void Init()
        {
            this.AddBinding(this.Data);
        }

        /// <summary>
        ///     Called by localization system when language changes.
        /// </summary>
        protected void OnLocalize()
        {
            this.UpdateValue();
        }

        /// <inheritdoc />
        protected override void UpdateValue()
        {
            this.OnValueChanged();
        }
    }
}