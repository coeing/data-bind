﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PopupListValueGetter.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.UI.NGUI.Getters
{
    using System;
    using Slash.Unity.DataBind.Foundation.Providers.Getters;

    using UnityEngine;

    /// <summary>
    ///   Gets the text value of a UIPopupList element.
    /// </summary>
    [AddComponentMenu("Data Bind/NGUI/Getters/[DB(Obsolete)] Popup List Value Getter (NGUI)")]
    [Obsolete("Use PopupListValueProvider instead")]
    public class PopupListValueGetter : ComponentSingleGetter<UIPopupList, string>
    {
        #region Methods
        
        /// <summary>
        ///     Register listener at target to be informed if its value changed.
        ///     The target is already checked for null reference.
        /// </summary>
        /// <param name="target">Target to add listener to.</param>
        protected override void AddListener(UIPopupList target)
        {
            EventDelegate.Add(target.onChange, this.OnTargetValueChanged);
        }

        /// <summary>
        ///     Derived classes should return the current value to set if this method is called.
        ///     The target is already checked for null reference.
        /// </summary>
        /// <param name="target">Target to get value from.</param>
        /// <returns>Current value to set.</returns>
        protected override string GetValue(UIPopupList target)
        {
            return target.value;
        }

        /// <summary>
        ///     Remove listener from target which was previously added in AddListener.
        ///     The target is already checked for null reference.
        /// </summary>
        /// <param name="target">Target to remove listener from.</param>
        protected override void RemoveListener(UIPopupList target)
        {
            EventDelegate.Remove(target.onChange, this.OnTargetValueChanged);
        }

        #endregion
    }
}