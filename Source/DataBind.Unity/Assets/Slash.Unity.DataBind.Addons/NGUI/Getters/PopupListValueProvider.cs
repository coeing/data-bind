﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PopupListValueProvider.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.UI.NGUI.Getters
{
    using Slash.Unity.DataBind.Foundation.Providers.Getters;
    using UnityEngine;

    /// <summary>
    ///     Provides the value of a UIPopupList element.
    /// </summary>
    [AddComponentMenu("Data Bind/NGUI/Getters/[DB] Popup List Value Provider (NGUI)")]
    public class PopupListValueProvider : ComponentDataProvider<UIPopupList, string>
    {
        /// <inheritdoc />
        protected override void AddListener(UIPopupList target)
        {
            EventDelegate.Add(target.onChange, this.OnTargetValueChanged);
        }

        /// <inheritdoc />
        protected override string GetValue(UIPopupList target)
        {
            return target.value;
        }

        /// <inheritdoc />
        protected override void RemoveListener(UIPopupList target)
        {
            EventDelegate.Remove(target.onChange, this.OnTargetValueChanged);
        }
    }
}