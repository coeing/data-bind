﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PopupListDataProvider.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.UI.NGUI.Getters
{
    using Slash.Unity.DataBind.Foundation.Providers.Getters;

    using UnityEngine;

    /// <summary>
    ///   Provides the data value of a UIPopupList element.
    /// </summary>
    [AddComponentMenu("Data Bind/NGUI/Getters/[DB] Popup List Data Provider (NGUI)")]
    public class PopupListDataProvider : ComponentDataProvider<UIPopupList, object>
    {
        /// <inheritdoc />
        protected override void AddListener(UIPopupList target)
        {
            EventDelegate.Add(target.onChange, this.OnTargetValueChanged);
        }

        /// <inheritdoc />
        protected override object GetValue(UIPopupList target)
        {
            return target.data;
        }

        /// <inheritdoc />
        protected override void RemoveListener(UIPopupList target)
        {
            EventDelegate.Remove(target.onChange, this.OnTargetValueChanged);
        }
    }
}