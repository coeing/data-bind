﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InputValueGetter.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.UI.NGUI.Getters
{
    using System;
    using Slash.Unity.DataBind.Foundation.Providers.Getters;

    using UnityEngine;

    /// <summary>
    ///   Gets the value of a UIInput element.
    /// </summary>
    [AddComponentMenu("Data Bind/NGUI/Getters/[DB(Obsolete)] Input Value Getter (NGUI)")]
    [Obsolete("Use InputValueProvider instead")]
    public class InputValueGetter : ComponentSingleGetter<UIInput, string>
    {
        #region Methods

        /// <summary>
        ///     Register listener at target to be informed if its value changed.
        ///     The target is already checked for null reference.
        /// </summary>
        /// <param name="target">Target to add listener to.</param>
        protected override void AddListener(UIInput target)
        {
            EventDelegate.Add(target.onChange, this.OnTargetValueChanged);
        }

        /// <summary>
        ///     Derived classes should return the current value to set if this method is called.
        ///     The target is already checked for null reference.
        /// </summary>
        /// <param name="target">Target to get value from.</param>
        /// <returns>Current value to set.</returns>
        protected override string GetValue(UIInput target)
        {
            return target.value;
        }

        /// <summary>
        ///     Remove listener from target which was previously added in AddListener.
        ///     The target is already checked for null reference.
        /// </summary>
        /// <param name="target">Target to remove listener from.</param>
        protected override void RemoveListener(UIInput target)
        {
            EventDelegate.Remove(target.onChange, this.OnTargetValueChanged);
        }

        #endregion
    }
}