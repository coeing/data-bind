﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InputValueProvider.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.UI.NGUI.Getters
{
    using Slash.Unity.DataBind.Foundation.Providers.Getters;
    using UnityEngine;

    /// <summary>
    ///     Provides the value of a UIInput element.
    /// </summary>
    [AddComponentMenu("Data Bind/NGUI/Getters/[DB] Input Value Provider (NGUI)")]
    public class InputValueProvider : ComponentDataProvider<UIInput, string>
    {
        /// <inheritdoc />
        protected override void AddListener(UIInput target)
        {
            EventDelegate.Add(target.onChange, this.OnTargetValueChanged);
        }

        /// <inheritdoc />
        protected override string GetValue(UIInput target)
        {
            return target.value;
        }

        /// <inheritdoc />
        protected override void RemoveListener(UIInput target)
        {
            EventDelegate.Remove(target.onChange, this.OnTargetValueChanged);
        }
    }
}