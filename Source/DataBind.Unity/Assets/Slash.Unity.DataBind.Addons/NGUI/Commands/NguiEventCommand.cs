﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NguiEventCommand.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.UI.NGUI.Commands
{
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;

    using Slash.Unity.DataBind.Foundation.Commands;

    using UnityEngine;

    /// <summary>
    ///   Base class for a command when a NGUI event on a widget occured.
    /// </summary>
    /// <typeparam name="TWidget">Type of widget to monitor.</typeparam>
    public abstract class NguiEventCommand<TWidget> : Command
        where TWidget : MonoBehaviour
    {
        #region Fields

        /// <summary>
        ///   Target widget to work with.
        /// </summary>
        public TWidget Target;

        #endregion

        #region Methods

        /// <summary>
        ///   Unity callback.
        /// </summary>
        protected override void Awake()
        {
            base.Awake();

            if (this.Target == null)
            {
                this.Target = this.GetComponent<TWidget>();
            }
        }

        /// <summary>
        ///   Returns the event from the specified target to observe.
        /// </summary>
        /// <param name="target">Target behaviour to get event from.</param>
        /// <returns>Event from the specified target to observe.</returns>
        protected abstract List<EventDelegate> GetEvent(TWidget target);

        /// <summary>
        ///   Unity callback.
        /// </summary>
        [SuppressMessage("ReSharper", "VirtualMemberNeverOverriden.Global")]
        protected virtual void OnDisable()
        {
            if (this.Target != null)
            {
                EventDelegate.Remove(this.GetEvent(this.Target), this.OnEvent);
            }
        }

        /// <summary>
        ///   Unity callback.
        /// </summary>
        [SuppressMessage("ReSharper", "UnusedMemberHiearchy.Global")]
        protected virtual void OnEnable()
        {
            if (this.Target != null)
            {
                EventDelegate.Add(this.GetEvent(this.Target), this.OnEvent);
            }
        }

        /// <summary>
        ///   Called when the observed event occured.
        /// </summary>
        protected virtual void OnEvent()
        {
            this.InvokeCommand();
        }

        #endregion
    }
}