﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PopupListChangeCommand.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.UI.NGUI.Commands
{
    using System.Collections.Generic;

    using UnityEngine;

    /// <summary>
    ///   Command which is triggered when the selected value of a popup list changed.
    /// </summary>
    [AddComponentMenu("Data Bind/NGUI/Commands/[DB] Popup List Change Command (NGUI)")]
    public class PopupListChangeCommand : NguiEventCommand<UIPopupList>
    {
        #region Fields

        /// <summary>
        ///   Storing popup value for comparison, the onChange event is triggered
        ///   if the same value is selected again, too.
        /// </summary>
        private string value;

        #endregion

        #region Methods

        /// <summary>
        ///   Returns the event from the specified target to observe.
        /// </summary>
        /// <param name="target">Target behaviour to get event from.</param>
        /// <returns>Event from the specified target to observe.</returns>
        protected override List<EventDelegate> GetEvent(UIPopupList target)
        {
            return target.onChange;
        }

        /// <summary>
        ///   Unity callback.
        /// </summary>
        protected override void OnEnable()
        {
            base.OnEnable();
            if (this.Target != null)
            {
                this.value = this.Target.value;
            }
        }

        /// <summary>
        ///   Called when the observed event occured.
        /// </summary>
        protected override void OnEvent()
        {
            var newValue = this.Target.value;
            if (newValue == this.value)
            {
                return;
            }

            base.OnEvent();
            this.value = newValue;
        }

        #endregion
    }
}