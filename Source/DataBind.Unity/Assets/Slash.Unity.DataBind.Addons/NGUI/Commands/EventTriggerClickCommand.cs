﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EventTriggerClickCommand.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.UI.NGUI.Commands
{
    using System.Collections.Generic;

    using UnityEngine;

    /// <summary>
    ///   Command which is invoked when a onClick event occured on an event trigger.
    /// </summary>
    [AddComponentMenu("Data Bind/NGUI/Commands/[DB] Event Trigger Click Command (NGUI)")]
    public class EventTriggerClickCommand : NguiEventCommand<UIEventTrigger>
    {
        #region Methods

        /// <summary>
        ///   Returns the event from the specified target to observe.
        /// </summary>
        /// <param name="target">Target behaviour to get event from.</param>
        /// <returns>Event from the specified target to observe.</returns>
        protected override List<EventDelegate> GetEvent(UIEventTrigger target)
        {
            return target.onClick;
        }

        #endregion
    }
}