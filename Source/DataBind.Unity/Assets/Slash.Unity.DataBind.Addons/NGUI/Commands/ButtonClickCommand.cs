﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ButtonClickCommand.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.UI.NGUI.Commands
{
    using System.Collections.Generic;

    using UnityEngine;

    /// <summary>
    ///   Command to invoke when a button was clicked.
    /// </summary>
    [AddComponentMenu("Data Bind/NGUI/Commands/[DB] Button Click Command (NGUI)")]
    public class ButtonClickCommand : NguiEventCommand<UIButton>
    {
        #region Methods

        /// <summary>
        ///   Returns the event from the specified target to observe.
        /// </summary>
        /// <param name="target">Target behaviour to get event from.</param>
        /// <returns>Event from the specified target to observe.</returns>
        protected override List<EventDelegate> GetEvent(UIButton target)
        {
            return target.onClick;
        }

        #endregion
    }
}