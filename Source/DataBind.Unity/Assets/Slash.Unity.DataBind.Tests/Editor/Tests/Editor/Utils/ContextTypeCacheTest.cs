﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ContextTypeCacheTest.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.Editor.Tests.Editor.Utils
{
    using System;
    using System.ComponentModel;
    using JetBrains.Annotations;
    using NUnit.Framework;
    using Slash.Unity.DataBind.Core.Data;
    using Slash.Unity.DataBind.Core.Utils;
    using Slash.Unity.DataBind.Editor.Utils;

    public class ContextTypeCacheTest
    {
        /// <summary>
        ///   Tests that paths for a class that implements INotifyPropertyChanged are correctly generated.
        /// </summary>
        [Test]
        public void PathsForINotifyPropertyChangedClass()
        {
            var paths = ContextTypeCache.GetPaths(typeof(TestNotifyPropertyChangedClass),
                ContextMemberFilter.Properties);
            Assert.GreaterOrEqual(paths.Count, 1);
            Assert.IsTrue(paths.Contains("Text"));
        }

        /// <summary>
        ///     Tests which members are returned for a collection.
        /// </summary>
        [Test]
        public void PathsForIntCollection()
        {
            var collectionType = typeof(Collection<int>);
            var paths = ContextTypeCache.GetPaths(collectionType, ContextMemberFilter.Properties);
            Assert.GreaterOrEqual(paths.Count, 1);
            Assert.IsTrue(paths.Contains("Count"));
        }

        /// <summary>
        ///   Tests that paths for a serializable class are correctly generated.
        /// </summary>
        [Test]
        public void PathsForSerializableClass()
        {
            var paths = ContextTypeCache.GetPaths(typeof(TestSerializableClass), ContextMemberFilter.Fields);
            Assert.GreaterOrEqual(paths.Count, 2);
            Assert.IsTrue(paths.Contains("Text"));
            Assert.IsTrue(paths.Contains("Number"));
        }

        /// <summary>
        ///     Tests if different paths are returned for two members with the same type.
        /// </summary>
        [Test]
        public void TestTwoMembersWithSameTypeNotEqualPath()
        {
            var paths = ContextTypeCache.GetPaths(typeof(TestContext), ContextMemberFilter.Properties);
            Assert.AreEqual(2, paths.Count);
            Assert.AreNotEqual(paths[0], paths[1]);
        }

        public class TestContext : Context
        {
            public int MemberA { get; set; }

            public int MemberB { get; set; }
        }

        private class TestNotifyPropertyChangedClass : INotifyPropertyChanged
        {
            private string text;

            public event PropertyChangedEventHandler PropertyChanged;

            public string Text
            {
                get
                {
                    return this.text;
                }
                set
                {
                    if (value == this.text)
                    {
                        return;
                    }
                    this.text = value;
                    this.OnPropertyChanged("Text");
                }
            }

            [NotifyPropertyChangedInvocator]
            protected virtual void OnPropertyChanged(string propertyName)
            {
                var handler = this.PropertyChanged;
                if (handler != null)
                {
                    handler(this, new PropertyChangedEventArgs(propertyName));
                }
            }
        }

        [Serializable]
        private class TestSerializableClass
        {
#pragma warning disable 649
            public int Number;
#pragma warning restore 649

#pragma warning disable 649
            public string Text;
#pragma warning restore 649
        }
    }
}