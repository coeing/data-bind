﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DataBindingTest.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Slash.Unity.DataBind.Editor.Tests.Core.Presentation
{
    using NUnit.Framework;
    using Slash.Unity.DataBind.Core.Presentation;
    using UnityEngine;

    public class DataBindingTest
    {
        private class TestMonoBehaviour : MonoBehaviour
        {
        }

        [Test]
        public void DontSetInitializedIfContextNotInitialized()
        {
            var dataBinding = new DataBinding {Type = DataBindingType.Context};

            var testGameObject = new GameObject();
            var testMonoBehaviour = testGameObject.AddComponent<TestMonoBehaviour>();
            dataBinding.Init(new DataContextNodeConnectorInitializer(), testMonoBehaviour);

            Assert.IsFalse(dataBinding.IsInitialized);
        }
    }
}