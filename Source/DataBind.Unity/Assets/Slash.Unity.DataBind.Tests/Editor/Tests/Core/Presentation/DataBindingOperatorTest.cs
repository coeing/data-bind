﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DataBindingOperatorTest.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.Tests.Core.Presentation
{
    using NUnit.Framework;
    using Slash.Unity.DataBind.Core.Presentation;
    using Slash.Unity.DataBind.Editor.Tests.Core.Foundation;
    using UnityEngine;

    public class DataBindingOperatorTest
    {
        [Test]
        public void CallbackFromAddBindingCalledOnDataChange()
        {
            var gameObject = new GameObject("DataBindingOperatorHolder");
            var dataBindingOperator = gameObject.AddComponent<DataBindingOperator>();

            // Create data provider.
            var testDataProvider = new TestDataProvider<bool> {TestValue = false};
            var dataBinding = new DataBinding {Type = DataBindingType.Provider};
            dataBinding.InitProvider(testDataProvider);

            // Add binding with callback.
            var valueChangedCalled = false;
            dataBindingOperator.AddBinding(dataBinding, () => valueChangedCalled = true);
            
            // Init and run data binding operator.
            dataBindingOperator.Init();
            dataBindingOperator.Enable();

            // Change value.
            testDataProvider.TestValue = true;

            Assert.IsTrue(valueChangedCalled, "Callback not invoked on value change");
        }
    }
}