﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ContextHolderTest.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Slash.Unity.DataBind.Editor.Tests.Core.Presentation
{
    using System.ComponentModel;
    using NUnit.Framework;
    using Slash.Unity.DataBind.Core.Data;
    using Slash.Unity.DataBind.Core.Presentation;
    using UnityEngine;

    public class ContextHolderTest
    {
        [Test]
        public void CapsuleINotifyPropertyChangedClassAutomatically()
        {
            var testContext = NSubstitute.Substitute.For<INotifyPropertyChanged>();

            var contextHolderGameObject = new GameObject();
            var contextHolder = contextHolderGameObject.AddComponent<ContextHolder>();
            contextHolder.Context = testContext;

            Assert.IsTrue(contextHolder.Context is NotifyPropertyChangedDataContext);
        }
    }
}