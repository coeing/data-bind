﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DataContextNodeConnectorInitializerTest.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.Tests.Core.Presentation
{
    using System;
    using NUnit.Framework;
    using Slash.Unity.DataBind.Core.Presentation;
    using UnityEngine;

    public class DataContextNodeConnectorInitializerTest
    {
        [Test]
        public void ShouldExecuteInitializationWhichWasRegisteredOnParentInitialization()
        {
            var initializer = new DataContextNodeConnectorInitializer();
            var gameObject = new GameObject();
            var contextHolder = gameObject.AddComponent<ContextHolder>();
            var connector = new DataContextNodeConnector(initializer,
                contextHolder, "Parent");
            var connectorChild = new DataContextNodeConnector(initializer,
                contextHolder, "Child");

            var initialValue = "43";
            connector.SetValueListener(value => { initializer.RegisterInitialization(connectorChild, initialValue); });

            object setValue = null;
            connectorChild.SetValueListener(value => setValue = value);

            initializer.RegisterInitialization(connector, "42");
            initializer.ExecuteInitializations();

            Assert.AreEqual(initialValue, setValue);
        }

        [Test]
        public void ShouldInitConnectorWhenInitializationsAreExecuted()
        {
            var initializer = new DataContextNodeConnectorInitializer();
            var gameObject = new GameObject();
            var connector = new DataContextNodeConnector(initializer,
                gameObject.AddComponent<ContextHolder>(), "Test");
            object setValue = null;
            connector.SetValueListener(value => setValue = value);
            var initialValue = "42";
            initializer.RegisterInitialization(connector, initialValue);
            initializer.ExecuteInitializations();

            Assert.AreEqual(initialValue, setValue);
        }

        [Test]
        public void ShouldNotInitConnectorWhenRegisteredInitializationWasRemoved()
        {
            var initializer = new DataContextNodeConnectorInitializer();
            var gameObject = new GameObject();
            var connector = new DataContextNodeConnector(initializer,
                gameObject.AddComponent<ContextHolder>(), "Test");
            object setValue = null;
            connector.SetValueListener(value => setValue = value);
            var initialValue = "42";
            initializer.RegisterInitialization(connector, initialValue);
            initializer.RemoveInitialization(connector);
            initializer.ExecuteInitializations();

            Assert.AreEqual(null, setValue);
        }

        [Test]
        public void ShouldThrowIfRegisterInitializationTwice()
        {
            var initializer = new DataContextNodeConnectorInitializer();
            var gameObject = new GameObject();
            var connector = new DataContextNodeConnector(initializer,
                gameObject.AddComponent<ContextHolder>(), "Test");
            initializer.RegisterInitialization(connector, "42");
            Assert.Throws<InvalidOperationException>(() => initializer.RegisterInitialization(connector, "43"));
        }
    }
}