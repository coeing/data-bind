﻿namespace Slash.Unity.DataBind.Editor.Tests.Core.Data
{
    using System;
    using Slash.Unity.DataBind.Core.Utils;

    public class TestDataBindingSettings : IDataBindSettings
    {
        public event Action<string> LoggedWarning;

        /// <inheritdoc />
        public bool AppendUnderscoreSetting
        {
            get
            {
                return false;
            }
        }

        /// <inheritdoc />
        public DataBindSettings.DataProviderFormats DataProviderFormatSetting
        {
            get
            {
                return DataBindSettings.DataProviderFormats.FirstLetterLowerCase;
            }
        }

        /// <inheritdoc />
        public bool DataProviderIsCaseInsensitive { get; set; }

        /// <inheritdoc />
        public bool ReportMissingBackingFields { get; set; }

        /// <inheritdoc />
        public string[] IgnoreContextTypesInNamespaces { get; set; }

        /// <inheritdoc />
        public void LogWarning(string log)
        {
            Console.WriteLine("WARNING: " + log);
            this.OnLoggedWarning(log);
        }

        protected virtual void OnLoggedWarning(string log)
        {
            var handler = this.LoggedWarning;
            if (handler != null)
            {
                handler(log);
            }
        }
    }
}