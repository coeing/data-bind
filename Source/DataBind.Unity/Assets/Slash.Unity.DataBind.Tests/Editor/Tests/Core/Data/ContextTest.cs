﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ContextTest.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.Editor.Tests.Core.Data
{
    using System;
    using NUnit.Framework;
    using Slash.Unity.DataBind.Core.Data;
    using Slash.Unity.DataBind.Core.Utils;

    public class ContextTest
    {
        private const string ExistingProperty = "Existing";

        private const string MissingProperty = "Missing";

        [Test]
        public void GetStructMemberValue()
        {
            DataBindingSettingsProvider.SettingsOverride = new TestDataBindingSettings();

            var testContext = new ContextWithStruct { Struct = new TestStruct { Number = 21 } };
            var value = testContext.GetValue("Struct.Number");

            Assert.AreEqual(value, 21);
        }

        [Test]
        public void GetCaseInsensitiveDataPropertyFieldWhenSettingIsSet()
        {
            DataBindingSettingsProvider.SettingsOverride =
                new TestDataBindingSettings() { DataProviderIsCaseInsensitive = true, ReportMissingBackingFields = true };

            var testContext = new ContextWithCaseInsensitiveProperty();

            string changedARStateValue = null;
            testContext.RegisterListener("ARState", newValue => changedARStateValue = (string)newValue);

            const string arStateValue = "state";
            testContext.ARState = arStateValue;

            Assert.NotNull(changedARStateValue);
            Assert.AreEqual(arStateValue, changedARStateValue);
        }

        /// <summary>
        ///     A method node has to update its delegate when the object that contains the method changed
        ///     as the delegate depends on the object.
        /// </summary>
        [Test]
        public void MethodNodeUpdatesItsValueWhen()
        {
            var testContext = new TestMethodParentContext();
            var methodValueChanged = false;
            testContext.RegisterListener("SubContext.TestMethod", newValue => { methodValueChanged = true; });

            testContext.SubContext = new TestMethodContext();

            Assert.IsTrue(methodValueChanged, "Method value didn't change although object containing method changed");
        }

        [Test]
        public void ReceiveChangeEventOnItemAddedToCollection()
        {
            var testContext = new TestCollectionContext();
            var eventReceived = false;
            object eventItem = null;
            testContext.RegisterListener(
                "Collection.0",
                item =>
                {
                    eventReceived = true;
                    eventItem = item;
                });

            // Add item to collection.
            var addItem = new TestContext();
            testContext.Collection.Add(addItem);

            Assert.IsTrue(eventReceived, "No event received on adding item to collection");
            Assert.AreEqual(addItem, eventItem);
        }

        [Test]
        public void SetNestedStructMemberValue()
        {
            DataBindingSettingsProvider.SettingsOverride = new TestDataBindingSettings();

            var testContext =
                new ContextWithNestedStruct
                {
                    NestedStruct = new NestedStruct { TestStruct = new TestStruct { Number = 21 } }
                };
            testContext.SetValue("NestedStruct.TestStruct.Number", 42);

            Assert.AreEqual(testContext.NestedStruct.TestStruct.Number, 42);
        }

        [Test]
        public void SetStructMemberValue()
        {
            DataBindingSettingsProvider.SettingsOverride = new TestDataBindingSettings();

            var testContext = new ContextWithStruct { Struct = new TestStruct { Number = 21 } };
            testContext.SetValue("Struct.Number", 42);

            Assert.AreEqual(testContext.Struct.Number, 42);
        }

        [Test]
        public void TestExceptionOnMissingProperty()
        {
            Context testContext = new TestContext();

            Assert.Throws<ArgumentException>(() => testContext.RegisterListener(MissingProperty, TestCallback));
        }

        [Test]
        public void TestExceptionOnMissingPropertyOfNullSubContext()
        {
            Context testContext = new TestContext();
            Assert.Throws<ArgumentException>(
                () => testContext.RegisterListener("SubContext." + MissingProperty, TestCallback));
        }

        [Test]
        public void TestExceptionOnSetValueForReadOnlyProperty()
        {
            var testContext = new TestContext();
            Assert.Throws<InvalidOperationException>(() => testContext.SetValue("ReadOnly", "SetItAnyway!"));
        }

        [Test]
        public void TestNoExceptionOnExistingPropertyOfNullSubContext()
        {
            Context testContext = new TestContext();
            testContext.RegisterListener("SubContext." + ExistingProperty, TestCallback);
        }

        [Test]
        public void TestNoExceptionOnSetValueForNormalProperty()
        {
            var testContext = new TestContext();
            testContext.SetValue("Existing", "ShouldSet");
        }

        [Test]
        public void TestNoExceptionRegisterReadOnlyProperty()
        {
            var testContext = new TestContext();
            testContext.RegisterListener("ReadOnly", value => { });
        }

        [Test]
        public void TestSetValueOfDataProperty()
        {
            var testContext = new TestContext();
            const string TestValue = "BlaBla";
            testContext.SetValue("ExistingDataProperty", TestValue);
            Assert.AreEqual(TestValue, testContext.ExistingDataProperty);
        }

        [Test]
        public void TestSetValueOfRawProperty()
        {
            var testContext = new TestContext();
            const string TestValue = "BlaBla";
            testContext.SetValue("Existing", TestValue);
            Assert.AreEqual(TestValue, testContext.Existing);
        }

        [Test]
        public void TestUpdateSubContextDataProperty()
        {
            var testContext = new TestContext();
            var subContext1 = new TestContext { ExistingDataProperty = "SubOne" };
            var subContext2 = new TestContext { ExistingDataProperty = "SubTwo" };
            testContext.SubContext = subContext1;

            var valueChanged = false;
            testContext.RegisterListener(
                "SubContext.ExistingDataProperty",
                value =>
                {
                    Assert.AreEqual(subContext2.ExistingDataProperty, value);
                    valueChanged = true;
                });

            // Set sub context 2.
            testContext.SubContext = subContext2;

            // Check that value changed.
            Assert.IsTrue(valueChanged, "Sub context data didn't change although sub context changed.");
        }



        /// <summary>
        ///     A data context automatically creates a data tree with data nodes for each registered data property.
        ///     Those data nodes are only removed (and with them the registered callbacks) when the Destroy() method of the 
        ///     context is called.
        /// </summary>
        [Test]
        public void LastRemoveListenerRemovesCallbacksOnDataProperty()
        {
            var testContext = new TestContext();
            testContext.RegisterListener("ExistingDataProperty", TestCallback);

            Assert.AreEqual(1, Property.GetValueChangedListenerCount(testContext.existingDataPropertyProperty));

            // Remove listener
            testContext.RemoveListener("ExistingDataProperty", TestCallback);

            Assert.AreEqual(0, Property.GetValueChangedListenerCount(testContext.existingDataPropertyProperty));
        }

        private static void TestCallback(object obj)
        {
        }

        private struct NestedStruct
        {
            public TestStruct TestStruct;
        }

        private class ContextWithNestedStruct : Context
        {
            private readonly Property<NestedStruct> nestedStructProperty =
                new Property<NestedStruct>();

            public NestedStruct NestedStruct
            {
                get
                {
                    return this.nestedStructProperty.Value;
                }
                set
                {
                    this.nestedStructProperty.Value = value;
                }
            }
        }

        private struct TestStruct
        {
            public int Number;

            public string Text;
        }

        private class ContextWithCaseInsensitiveProperty : Context
        {
            private readonly Property<string> arStateProperty =
                new Property<string>();

            public string ARState
            {
                get
                {
                    return this.arStateProperty.Value;
                }
                set
                {
                    this.arStateProperty.Value = value;
                }
            }
        }

        private class ContextWithStruct : Context
        {
            private readonly Property<TestStruct> structProperty =
                new Property<TestStruct>();

            public TestStruct Struct
            {
                get
                {
                    return this.structProperty.Value;
                }
                set
                {
                    this.structProperty.Value = value;
                }
            }
        }

        private class TestMethodParentContext : Context
        {
            private readonly Property<TestMethodContext> subContextProperty = new Property<TestMethodContext>();

            public TestMethodContext SubContext
            {
                get
                {
                    return this.subContextProperty.Value;
                }
                set
                {
                    this.subContextProperty.Value = value;
                }
            }
        }

        private class TestMethodContext : Context
        {
            public void TestMethod()
            {
            }
        }

        private class TestCollectionContext : Context
        {
            #region Fields

            private readonly Property<Collection<TestContext>> collectionProperty =
                new Property<Collection<TestContext>>(new Collection<TestContext>());

            #endregion

            #region Properties

            public Collection<TestContext> Collection
            {
                get
                {
                    return this.collectionProperty.Value;
                }
                set
                {
                    this.collectionProperty.Value = value;
                }
            }

            #endregion
        }

        public class TestContext : Context
        {
            #region Fields

            public readonly Property<string> existingDataPropertyProperty = new Property<string>();

            private readonly Property<TestContext> subContextProperty = new Property<TestContext>();

            #endregion

            #region Properties

            public string Existing { get; set; }

            public string ExistingDataProperty
            {
                get
                {
                    return this.existingDataPropertyProperty.Value;
                }
                set
                {
                    this.existingDataPropertyProperty.Value = value;
                }
            }

            public string ReadOnly
            {
                get
                {
                    return "ReadOnly";
                }
            }

            public TestContext SubContext
            {
                get
                {
                    return this.subContextProperty.Value;
                }
                set
                {
                    this.subContextProperty.Value = value;
                }
            }

            #endregion
        }
    }
}