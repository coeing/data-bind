﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CollectionTest.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.Editor.Tests.Core.Data
{
    using NUnit.Framework;
    using Slash.Unity.DataBind.Core.Data;
    using Slash.Unity.DataBind.Core.Utils;

    public class CollectionTest
    {
        [Test]
        public void GenericEnumeratorReturnsItemOfType()
        {
            var collection = new Collection<TestClass> {new TestClass {Name = "A"}};
            foreach (var item in collection)
            {
                Assert.AreEqual("A", item.Name);
            }
        }

        [Test]
        public void GenericIndexerReturnsItemOfType()
        {
            var collection = new Collection<TestClass> {new TestClass {Name = "A"}};
            var item = collection[0];
            Assert.AreEqual("A", item.Name);
        }

        [Test]
        public void NonGenericEnumerator()
        {
            Collection collection = new Collection<int> {3, 2, 1};
            foreach (var item in collection)
            {
                Assert.IsInstanceOf<int>(item);
            }
        }

        [Test]
        public void NonGenericIndexer()
        {
            Collection collection = new Collection<int> {3, 2, 1};
            var item = collection[0];
            Assert.IsInstanceOf<int>(item);
        }

        private class TestClass
        {
            public string Name;
        }		
		
        public class CollectionTestContext : Context
        {
            private readonly Property<Collection<int>>
                collectionProperty = new Property<Collection<int>>(
                    new Collection<int>());

            public Collection<int> Collection
            {
                get { return this.collectionProperty.Value; }
                set { this.collectionProperty.Value = value; }
            }

        }

        [Test]
        public void NoItemAddedEventAfterCountChangeEvent()
        {
            DataBindingSettingsProvider.SettingsOverride = new TestDataBindingSettings();

            CollectionTestContext context = new CollectionTestContext();
            bool itemAddedCalled = false;
            context.RegisterListener("Collection.Count", count =>
            {
                // Register for ItemAdded event.
                context.Collection.ItemAdded += item => itemAddedCalled = true;
            });

            // Add item.
            context.Collection.Add(1);

            Assert.IsFalse(itemAddedCalled);
        }
    }
}