// --------------------------------------------------------------------------------------------------------------------
// <copyright file="XmlSerializationTest.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.Tests.Core.Data
{
    using System.IO;
    using System.Linq;
    using System.Xml.Serialization;
    using NUnit.Framework;
    using Slash.Unity.DataBind.Core.Data;

    public class XmlSerializationTest
    {
        [Test]
        public void SerializeContextWithCollection()
        {
            var context = new ContextWithCollection {StringCollection = new Collection<string> {"a", "c", "b"}};
            TestSerialize(context);
        }

        [Test]
        public void SerializeContextWithProperty()
        {
            var context = new ContextWithProperty {Int = 23};
            TestSerialize(context);
        }

        private static void TestSerialize(object context)
        {
            var stringWriter = new StringWriter();
            var xmlSerializer = new XmlSerializer(context.GetType());
            xmlSerializer.Serialize(stringWriter, context);

            var xml = stringWriter.ToString();
            var deserializedContext = xmlSerializer.Deserialize(new StringReader(xml));

            Assert.AreEqual(context, deserializedContext);
        }

        public class ContextWithProperty : Context
        {
            private readonly Property<int> intProperty =
                new Property<int>();

            public int Int
            {
                get
                {
                    return this.intProperty.Value;
                }
                set
                {
                    this.intProperty.Value = value;
                }
            }

            /// <inheritdoc />
            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj))
                {
                    return false;
                }

                if (ReferenceEquals(this, obj))
                {
                    return true;
                }

                if (obj.GetType() != this.GetType())
                {
                    return false;
                }

                return this.Equals((ContextWithProperty) obj);
            }

            /// <inheritdoc />
            public override int GetHashCode()
            {
                return this.Int;
            }

            protected bool Equals(ContextWithProperty other)
            {
                return Equals(this.Int, other.Int);
            }
        }

        public class ContextWithCollection : Context
        {
            private readonly Property<Collection<string>>
                stringCollectionProperty = new Property<Collection<string>>(
                    new Collection<string>());

            public Collection<string> StringCollection
            {
                get
                {
                    return this.stringCollectionProperty.Value;
                }
                set
                {
                    this.stringCollectionProperty.Value = value;
                }
            }

            /// <inheritdoc />
            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj))
                {
                    return false;
                }

                if (ReferenceEquals(this, obj))
                {
                    return true;
                }

                if (obj.GetType() != this.GetType())
                {
                    return false;
                }

                return this.Equals((ContextWithCollection) obj);
            }

            /// <inheritdoc />
            public override int GetHashCode()
            {
                return this.stringCollectionProperty != null ? this.stringCollectionProperty.GetHashCode() : 0;
            }

            protected bool Equals(ContextWithCollection other)
            {
                return this.StringCollection.SequenceEqual(other.StringCollection);
            }
        }
    }
}