﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DataContextNodeConnectorTest.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.Editor.Tests.Core.Data
{
    using System;
    using NUnit.Framework;
    using Slash.Unity.DataBind.Core.Data;
    using Slash.Unity.DataBind.Core.Presentation;
    using Slash.Unity.DataBind.Foundation.Setters;
    using UnityEngine;

    [TestFixture]
    public class DataContextNodeConnectorTest
    {
        private class ContextWithCollection : TestContext
        {
            #region Fields

            private readonly Property<Collection<TestContext>> collectionProperty =
                new Property<Collection<TestContext>>(new Collection<TestContext>());

            #endregion

            #region Properties

            public Collection<TestContext> Collection
            {
                get
                {
                    return this.collectionProperty.Value;
                }
                set
                {
                    this.collectionProperty.Value = value;
                }
            }

            #endregion
        }

        public class TestContext : Context
        {
            #region Fields

            private readonly Property<string> nameProperty = new Property<string>();

            private readonly Property<TestContext> subContext2Property = new Property<TestContext>();

            private readonly Property<TestContext> subContextProperty = new Property<TestContext>();

            #endregion

            #region Properties

            public string Name
            {
                get
                {
                    return this.nameProperty.Value;
                }
                set
                {
                    this.nameProperty.Value = value;
                }
            }

            public TestContext SubContext
            {
                get
                {
                    return this.subContextProperty.Value;
                }
                set
                {
                    this.subContextProperty.Value = value;
                }
            }

            public TestContext SubContext2
            {
                get
                {
                    return this.subContext2Property.Value;
                }
                set
                {
                    this.subContext2Property.Value = value;
                }
            }

            #endregion
        }

        [Test]
        public void ShouldCallbackWithInitialValueOnInitialization()
        {
            var testContext = new TestContext {Name = "Root"};
            var rootGameObject = new GameObject();
            var contextHolder = rootGameObject.AddComponent<ContextHolder>();
            contextHolder.SetContext(testContext, null);

            var nodeConnector =
                new DataContextNodeConnector(new DataContextNodeConnectorInitializer(), contextHolder, "Name");
            object setValue = null;
            nodeConnector.SetValueListener(value => setValue = value);
            var initialValue = "Test";
            nodeConnector.Init(initialValue);

            Assert.AreEqual(initialValue, setValue);
        }

        [Test]
        public void ShouldInitItselfViaInitializer()
        {
            var testContext = new TestContext { Name = "Root" };
            var rootGameObject = new GameObject();
            var contextHolder = rootGameObject.AddComponent<ContextHolder>();
            contextHolder.SetContext(testContext, null);

            var initializer = new DataContextNodeConnectorInitializer();
            var nodeConnector =
                new DataContextNodeConnector(initializer, contextHolder, "Name");
            initializer.ExecuteInitializations();

            Assert.IsTrue(nodeConnector.IsInitialized);
        }

        /**
         * Issue #92: An initialization was registered multiple times
         */
        [Test]
        public void ShouldNotThrowIfContextIsSetMultipleTimesInSameFrame()
        {
            var testContext1 = new TestContext { Name = "Root1" };
            var testContext2 = new TestContext { Name = "Root2" };
            var rootGameObject = new GameObject();
            var contextHolder = rootGameObject.AddComponent<ContextHolder>();
            contextHolder.Context = testContext1;

            var initializer = new DataContextNodeConnectorInitializer();
            var nodeConnector =
                new DataContextNodeConnector(initializer, contextHolder, "Name");

            // Set another context
            Assert.DoesNotThrow(() =>
            {
                contextHolder.Context = testContext2;
                nodeConnector.OnHierarchyChanged();
            });
        }

        [Test]
        public void ShouldThrowIfNoMonoBehaviourProvided()
        {
            var initializer = new DataContextNodeConnectorInitializer();
            Assert.Throws<ArgumentNullException>(() => new DataContextNodeConnector(initializer, null, "Name"));
        }

        [Test]
        public void ShouldThrowIfNoInitializerProvided()
        {
            var contextHolderGameObject = new GameObject();
            var contextHolder = contextHolderGameObject.AddComponent<ContextHolder>();
            Assert.Throws<ArgumentNullException>(() => new DataContextNodeConnector(null, contextHolder, "Name"));
        }

        [Test]
        public void ShouldNotInitItselfWhenNoDataContextFound()
        {
            var initializer = new DataContextNodeConnectorInitializer();
            var contextHolderGameObject = new GameObject();
            var contextHolder = contextHolderGameObject.AddComponent<ContextHolder>();
            var nodeConnector =
                new DataContextNodeConnector(initializer, contextHolder, "Name");
            initializer.ExecuteInitializations();

            Assert.IsFalse(nodeConnector.IsInitialized);
        }

        /// <summary>
        ///     Checks that a special path will return the context itself instead of one of its data properties.
        /// </summary>
        [Test]
        public void TestGetContextItself()
        {
            var testContext = new TestContext {Name = "Root"};

            var contextHolderGameObject = new GameObject();
            var contextHolder = contextHolderGameObject.AddComponent<ContextHolder>();
            contextHolder.SetContext(testContext, null);

            // Create context node.
            var node = new DataContextNodeConnector(new DataContextNodeConnectorInitializer(), contextHolder, "#");
            var nodeValue = (TestContext) node.SetValueListener(value => { });

            Assert.AreEqual(testContext, nodeValue);
        }

        [Test]
        public void TestGetParentContext()
        {
            var subSubContext = new TestContext {Name = "SubSub"};
            var subContext = new TestContext {SubContext = subSubContext, Name = "Sub"};
            var testContext = new TestContext {SubContext = subContext, Name = "Root"};

            var rootGameObject = new GameObject();
            var contextHolder = rootGameObject.AddComponent<ContextHolder>();
            contextHolder.SetContext(testContext, null);

            var subGameObject = new GameObject();
            subGameObject.transform.parent = rootGameObject.transform;
            var subMasterPath = subGameObject.AddComponent<MasterPath>();
            subMasterPath.Path = "SubContext";

            var subSubGameObject = new GameObject();
            subSubGameObject.transform.parent = subGameObject.transform;
            var subSubContextHolder = subSubGameObject.AddComponent<ContextHolder>();
            subSubContextHolder.SetContext(subSubContext, "SubContext");

            // Create context nodeConnector.
            var nodeConnector = new DataContextNodeConnector(new DataContextNodeConnectorInitializer(),
                subSubContextHolder, "#1.SubContext.Name");
            var nodeValue = (string) nodeConnector.SetValueListener(value => { });

            Assert.AreEqual("SubSub", nodeValue);
        }

        [Test]
        public void TestGetParentContextTwoMasterPaths()
        {
            var level3Context = new TestContext {Name = "Level 3"};
            var level2Context = new TestContext {SubContext = level3Context, Name = "Level 2"};
            var level1Context = new TestContext {SubContext = level2Context, Name = "Level 1"};
            var rootContext = new TestContext {SubContext = level1Context, Name = "Root"};

            var rootGameObject = new GameObject();
            var contextHolder = rootGameObject.AddComponent<ContextHolder>();
            contextHolder.SetContext(rootContext, null);

            var level1GameObject = new GameObject();
            level1GameObject.transform.parent = rootGameObject.transform;
            var level1MasterPath = level1GameObject.AddComponent<MasterPath>();
            level1MasterPath.Path = "SubContext";

            var level2GameObject = new GameObject();
            level2GameObject.transform.parent = level1GameObject.transform;
            var level2MasterPath = level2GameObject.AddComponent<MasterPath>();
            level2MasterPath.Path = "SubContext";

            var level3GameObject = new GameObject();
            level3GameObject.transform.parent = level2GameObject.transform;
            var level3ContextHolder = level3GameObject.AddComponent<ContextHolder>();
            level3ContextHolder.SetContext(level3Context, "SubContext");

            // Create context nodeConnector.
            var nodeConnector = new DataContextNodeConnector(new DataContextNodeConnectorInitializer(),
                level3ContextHolder, "#2.SubContext.Name");
            var nodeValue = (string) nodeConnector.SetValueListener(value => { });

            Assert.AreEqual("Level 2", nodeValue);
        }

        [Test]
        public void TestGetRootContext()
        {
            var subContext = new TestContext {Name = "Sub"};
            var testContext = new TestContext {SubContext = subContext, Name = "Root"};

            var rootGameObject = new GameObject();
            var contextHolder = rootGameObject.AddComponent<ContextHolder>();
            contextHolder.SetContext(testContext, null);

            var subGameObject = new GameObject();
            subGameObject.transform.parent = rootGameObject.transform;
            var subContextHolder = subGameObject.AddComponent<ContextHolder>();
            subContextHolder.SetContext(subContext, "SubContext");

            // Create context nodeConnector.
            var nodeConnector =
                new DataContextNodeConnector(new DataContextNodeConnectorInitializer(), subContextHolder, "#1.Name");
            var nodeValue = (string) nodeConnector.SetValueListener(value => { });

            Assert.AreEqual("Root", nodeValue);
        }

        [Test]
        public void TestRelativePathMasterPathBetweenThreeContexts()
        {
            var level3Context = new TestContext {Name = "Level 3"};
            var level2Context = new TestContext {SubContext = level3Context, Name = "Level 2"};
            var level1Context = new TestContext {SubContext = level2Context, Name = "Level 1"};
            var rootContext = new TestContext {SubContext = level1Context, Name = "Root"};

            var rootGameObject = new GameObject();
            var contextHolder = rootGameObject.AddComponent<ContextHolder>();
            contextHolder.SetContext(rootContext, null);

            var level1GameObject = new GameObject();
            level1GameObject.transform.parent = rootGameObject.transform;
            var level1ContextHolder = level1GameObject.AddComponent<ContextHolder>();
            level1ContextHolder.SetContext(level1Context, "SubContext");

            var level2GameObject = new GameObject();
            level2GameObject.transform.parent = level1GameObject.transform;
            var level2MasterPath = level2GameObject.AddComponent<MasterPath>();
            level2MasterPath.Path = "SubContext";

            var level3GameObject = new GameObject();
            level3GameObject.transform.parent = level2GameObject.transform;
            var level3ContextHolder = level3GameObject.AddComponent<ContextHolder>();
            level3ContextHolder.SetContext(level3Context, "SubContext");

            // Create context nodeConnector.
            var nodeConnector = new DataContextNodeConnector(new DataContextNodeConnectorInitializer(),
                level3ContextHolder, "#2.SubContext.Name");
            var nodeValue = (string) nodeConnector.SetValueListener(value => { });

            Assert.AreEqual("Level 2", nodeValue);
        }

        [Test]
        public void TestRelativePathTwoMasterPathsBetweenTwoContexts()
        {
            var level3Context = new TestContext {Name = "Level 3"};
            var level2Context = new TestContext {SubContext = level3Context, Name = "Level 2"};
            var level1Context = new TestContext {SubContext2 = level2Context, Name = "Level 1"};
            var rootContext = new TestContext {SubContext = level1Context, Name = "Root"};

            var rootGameObject = new GameObject();
            var contextHolder = rootGameObject.AddComponent<ContextHolder>();
            contextHolder.SetContext(rootContext, null);

            var level1GameObject = new GameObject();
            level1GameObject.transform.parent = rootGameObject.transform;
            var level1MasterPath = level1GameObject.AddComponent<MasterPath>();
            level1MasterPath.Path = "SubContext";

            var level2GameObject = new GameObject();
            level2GameObject.transform.parent = level1GameObject.transform;
            var level2MasterPath = level2GameObject.AddComponent<MasterPath>();
            level2MasterPath.Path = "SubContext2";

            var level3GameObject = new GameObject();
            level3GameObject.transform.parent = level2GameObject.transform;
            var level3ContextHolder = level3GameObject.AddComponent<ContextHolder>();
            level3ContextHolder.SetContext(level3Context, "SubContext");

            // Create context nodeConnector.
            var nodeConnector = new DataContextNodeConnector(new DataContextNodeConnectorInitializer(),
                level3ContextHolder, "#1.Name");
            var nodeValue = (string) nodeConnector.SetValueListener(value => { });

            Assert.AreEqual("Level 2", nodeValue);
        }

        [Test]
        public void TestRelativePathWithTwoPartCollectionPath()
        {
            var contextWithCollection = new ContextWithCollection
            {
                Name = "The Collection",
                Collection =
                    new Collection<TestContext>
                    {
                        new TestContext {Name = "Item 1"},
                        new TestContext {Name = "Item 2"}
                    }
            };
            var testContext = new TestContext {Name = "Root", SubContext = contextWithCollection};

            var rootGameObject = new GameObject();
            var contextHolder = rootGameObject.AddComponent<ContextHolder>();
            contextHolder.SetContext(testContext, null);

            var subGameObject = new GameObject();
            subGameObject.transform.parent = rootGameObject.transform;
            var slotItemsSetter = subGameObject.AddComponent<SlotItemsSetter>();
            slotItemsSetter.Data = new DataBinding {Type = DataBindingType.Context, Path = "SubContext.Collection"};

            var subSubGameObject = new GameObject();
            subSubGameObject.transform.parent = subGameObject.transform;
            var subSubContextHolder = subSubGameObject.AddComponent<ContextHolder>();
            subSubContextHolder.SetContext(contextWithCollection.Collection[0], "SubContext.Collection.0");

            // Create context nodeConnector.
            var nodeConnector = new DataContextNodeConnector(new DataContextNodeConnectorInitializer(),
                subSubContextHolder, "#2.Name");
            var nodeValue = (string) nodeConnector.SetValueListener(value => { });

            Assert.AreEqual(contextWithCollection.Name, nodeValue);
        }
    }
}