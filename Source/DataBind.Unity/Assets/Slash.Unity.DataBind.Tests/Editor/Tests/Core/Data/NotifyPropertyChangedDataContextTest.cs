﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NotifyPropertyChangedDataContextTest.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.Editor.Tests.Core.Data
{
    using System;
    using System.ComponentModel;
    using JetBrains.Annotations;
    using NUnit.Framework;
    using Slash.Unity.DataBind.Core.Data;
    using Slash.Unity.DataBind.Core.Utils;

    public class NotifyPropertyChangedDataContextTest
    {
        /// <summary>
        ///     Checks that the value of a property in a sub context can be received.
        /// </summary>
        [Test]
        public void FindSubNPCDContextProperty()
        {
            // Load settings from object, not from resource file.
            DataBindingSettingsProvider.SettingsOverride = new TestDataBindingSettings();

            // Create test context that has a sub context which is a NPCDContext.
            var testContext = new ContextWithNotifyPropertyChangedProperty();
            var testText = "Blabla";
            var subDataContext = new NotifyPropertyChangedClass {StringProperty = testText};
            testContext.Sub = subDataContext;

            var stringValue = testContext.GetValue("Sub.StringProperty");

            Assert.AreEqual(testText, stringValue);
        }

        [Test]
        public void ReceiveCallbackOnDataChange()
        {
            ReceiveCallbackOnDataChange(new NotifyPropertyChangedClass {StringProperty = "Initial"}, "StringProperty",
                data =>
                    data.StringProperty = "NewValue");
        }

        public static void ReceiveCallbackOnDataChange<TData>(TData data, string propertyName,
            Action<TData> setNewValue) where TData : INotifyPropertyChanged
        {
            // Load settings from object, not from resource file.
            DataBindingSettingsProvider.SettingsOverride = new TestDataBindingSettings();

            // Wrap in context.
            var context = new NotifyPropertyChangedDataContext(data);

            var dataValueChanged = false;
            context.RegisterListener(propertyName, newValue => { dataValueChanged = true; });
            setNewValue(data);

            Assert.IsTrue(dataValueChanged, "Didn't receive callback although data in context changed");
        }

        /// <summary>
        ///     Checks if a callback is received when the value of a property in a sub context changes.
        /// </summary>
        [Test]
        public void ReceiveCallbackOnDataChangeOfSubContextProperty()
        {
            // Load settings from object, not from resource file.
            DataBindingSettingsProvider.SettingsOverride = new TestDataBindingSettings();

            // Create test context that has a sub context which is a NPCDContext.
            var testContext = new ContextWithNotifyPropertyChangedProperty();
            var testText = "Blabla";
            var subDataContext = new NotifyPropertyChangedClass {StringProperty = testText};
            testContext.Sub = subDataContext;

            var receivedCallback = false;
            var stringValue = testContext.RegisterListener("Sub.StringProperty", newValue => receivedCallback = true);

            Assert.AreEqual(testText, stringValue);

            // Change string.
            subDataContext.StringProperty = "Change!";

            Assert.IsTrue(receivedCallback);
        }

        private class ContextWithNotifyPropertyChangedProperty : Context
        {
            private readonly Property<NotifyPropertyChangedClass> subProperty =
                new Property<NotifyPropertyChangedClass>();

            public NotifyPropertyChangedClass Sub
            {
                get
                {
                    return this.subProperty.Value;
                }
                set
                {
                    this.subProperty.Value = value;
                }
            }
        }

        private class NotifyPropertyChangedClass : INotifyPropertyChanged
        {
            private string stringProperty;

            public event PropertyChangedEventHandler PropertyChanged;

            public string StringProperty
            {
                get
                {
                    return this.stringProperty;
                }
                set
                {
                    if (value == this.stringProperty)
                    {
                        return;
                    }
                    this.stringProperty = value;

                    this.OnPropertyChanged("StringProperty");
                }
            }

            [NotifyPropertyChangedInvocator]
            protected virtual void OnPropertyChanged(string propertyName)
            {
                var handler = this.PropertyChanged;
                if (handler != null)
                {
                    handler(this, new PropertyChangedEventArgs(propertyName));
                }
            }
        }
    }
}