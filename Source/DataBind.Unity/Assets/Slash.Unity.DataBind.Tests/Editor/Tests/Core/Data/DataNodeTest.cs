﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DataNodeTest.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.Editor.Tests.Core.Data
{
    using NUnit.Framework;
    using Slash.Unity.DataBind.Core.Data;
    using Slash.Unity.DataBind.Core.Utils;

    public class DataNodeTest
    {
        [Test]
        public void DontCallbackWhenSameStringSet()
        {
            var testDataContextNode =
                new LeafDataNode(new ObjectNodeTypeInfo("Test") {Type = typeof(string)}, string.Empty);

            var valueChangedCalled = false;
            testDataContextNode.ValueChanged += newValue => valueChangedCalled = true;

            // Set same string, but with different reference.
            testDataContextNode.SetValue("aTest".Substring(1));

            Assert.IsFalse(valueChangedCalled, "Callback although string didn't change");
        }

        [Test]
        public void ReportMissingBackingField()
        {
            var settings = new TestDataBindingSettings {ReportMissingBackingFields = true};
            var warnings = RegisterListenerForPropertyWithoutBackingField(settings);
            Assert.AreEqual(1, warnings);
        }

        [Test]
        public void ReportMissingBackingFieldOff()
        {
            var settings = new TestDataBindingSettings {ReportMissingBackingFields = false};
            var warnings = RegisterListenerForPropertyWithoutBackingField(settings);
            Assert.AreEqual(0, warnings);
        }

        /// <summary>
        ///     Test if event handler for a data provider is correctly unregistered, when data node is not used anymore.
        /// </summary>
        [Test]
        public void UnregisterValueChangedEventCorrectly()
        {
            var testContext1 = new EventCountingContext();
            var testContext2 = new EventCountingContext();
            var rootContext = new RootEventCountingContext {Parent = testContext1};
            var testContext1ChildProperty = testContext1.ChildProperty;
            var testContext2ChildProperty = testContext2.ChildProperty;

            var rootNode = new BranchDataNode(new ObjectNodeTypeInfo(rootContext), "Root");

            // Get parent and child node to make sure they are created.
            var parentNode = rootNode.FindDescendant("Parent");
            parentNode.FindDescendant("Child");

            Assert.AreEqual(1, testContext1ChildProperty.EventHandlerCount);
            Assert.AreEqual(0, testContext2ChildProperty.EventHandlerCount);

            // Change value of parent node.
            parentNode.SetValue(testContext2);

            Assert.AreEqual(1, testContext2ChildProperty.EventHandlerCount);
            Assert.AreEqual(0, testContext1ChildProperty.EventHandlerCount);
        }

        private static int RegisterListenerForPropertyWithoutBackingField(TestDataBindingSettings settings)
        {
            var warnings = 0;
            settings.LoggedWarning += log => ++warnings;
            DataBindingSettingsProvider.SettingsOverride = settings;

            const string textValue = "InitialValue";
            var context = new ContextWithoutBackingField {Text = textValue};
            var initialValue = context.RegisterListener("Text", o => { });
            Assert.AreEqual(textValue, initialValue);

            return warnings;
        }

        private class EventCountingDataProvider : IDataProvider
        {
            public int EventHandlerCount;

            private object value;

            /// <inheritdoc />
            public event ValueChangedDelegate ValueChanged
            {
                add
                {
                    this.valueChanged += value;
                    ++this.EventHandlerCount;
                }
                remove
                {
                    this.valueChanged -= value;
                    --this.EventHandlerCount;
                }
            }

            // ReSharper disable once InconsistentNaming
            private event ValueChangedDelegate valueChanged;

            /// <inheritdoc />
            public bool IsInitialized
            {
                get
                {
                    return true;
                }
            }

            /// <inheritdoc />
            public object Value
            {
                get
                {
                    return this.value;
                }
                set
                {
                    if (value == this.value)
                    {
                        return;
                    }

                    this.value = value;

                    this.OnValueChanged();
                }
            }

            protected virtual void OnValueChanged()
            {
                var handler = this.valueChanged;
                if (handler != null)
                {
                    handler();
                }
            }
        }

        private class EventCountingContext : Context
        {
            private readonly EventCountingDataProvider childProperty =
                new EventCountingDataProvider();

            public object Child
            {
                get
                {
                    return this.childProperty.Value;
                }
                set
                {
                    this.childProperty.Value = value;
                }
            }

            public EventCountingDataProvider ChildProperty
            {
                get
                {
                    return this.childProperty;
                }
            }
        }

        private class RootEventCountingContext : Context
        {
            private readonly Property<EventCountingContext> parentProperty =
                new Property<EventCountingContext>();

            public EventCountingContext Parent
            {
                get
                {
                    return this.parentProperty.Value;
                }
                set
                {
                    this.parentProperty.Value = value;
                }
            }
        }

        private class ContextWithoutBackingField : Context
        {
            public string Text { get; set; }
        }
    }
}