﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TypeInfoUtilsTests.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.Editor.Tests.Core.Utils
{
    using System.Reflection;
    using NUnit.Framework;
    using Slash.Unity.DataBind.Core.Utils;

    public class TypeInfoUtilsTests
    {
        [Test]
        public void GetCaseInsensitiveFieldWithAdditionalFlag()
        {
            var fieldInfo =
                TypeInfoUtils.GetPrivateField(typeof(ClassWithOnePrivateField), "asingletext", BindingFlags.IgnoreCase);
            Assert.NotNull(fieldInfo);
            Assert.AreEqual("aSingleText", fieldInfo.Name);
        }

        [Test]
        public void GetCaseInsensitivePropertyWithAdditionalFlag()
        {
            var propertyInfo =
                TypeInfoUtils.GetPublicProperty(typeof(ClassWithOnePublicProperty), "asingletext",
                    BindingFlags.IgnoreCase);
            Assert.NotNull(propertyInfo);
            Assert.AreEqual("ASingleText", propertyInfo.Name);
        }

        private class ClassWithOnePrivateField
        {
            private string aSingleText;
        }

        private class ClassWithOnePublicProperty
        {
            public string ASingleText { get; set; }
        }
    }
}