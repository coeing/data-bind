// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ReflectionUtilsTests.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.Tests.Core.Utils
{
    using NUnit.Framework;
    using Slash.Unity.DataBind.Core.Utils;

    public class ReflectionUtilsTests
    {
        public class TryConvertValue
        {
            [Test]
            public void ConvertStringToEnum()
            {
                const TestEnum enumValue = TestEnum.Enum1;
                var stringValue = enumValue.ToString();
                TestEnum convertedValue;
                var converted = ReflectionUtils.TryConvertValue(stringValue, out convertedValue);
                Assert.IsTrue(converted);
                Assert.AreEqual(convertedValue, enumValue);
            }
            
            [Test]
            public void DontConvertInvalidStringToEnum()
            {
                const string stringValue = "InvalidEnumValue";
                TestEnum convertedValue;
                var converted = ReflectionUtils.TryConvertValue(stringValue, out convertedValue);
                Assert.IsFalse(converted);
            }
            
            [Test]
            public void ConvertIntToEnum()
            {
                const TestEnum enumValue = TestEnum.Enum1;
                const int intValue = (int) enumValue;
                TestEnum convertedValue;
                var converted = ReflectionUtils.TryConvertValue(intValue, out convertedValue);
                Assert.IsTrue(converted);
                Assert.AreEqual(convertedValue, enumValue);
            }
            
            [Test]
            public void DontConvertInvalidIntToEnum()
            {
                const int intValue = 10;
                TestEnum convertedValue;
                var converted = ReflectionUtils.TryConvertValue(intValue, out convertedValue);
                Assert.IsFalse(converted);
            }

            [Test]
            public void ConvertStringToFloat()
            {
                float convertedValue;
                var converted = ReflectionUtils.TryConvertValue("0.5", out convertedValue);
                Assert.IsTrue(converted);
                Assert.AreEqual(0.5, convertedValue);
            }

            private enum TestEnum
            {
                Enum0,

                Enum1,

                Enum2
            }
        }
    }
}