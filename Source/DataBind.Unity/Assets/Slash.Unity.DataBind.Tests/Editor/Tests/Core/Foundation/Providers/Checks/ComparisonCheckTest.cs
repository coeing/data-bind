// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ComparisonCheckTest.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------


namespace Slash.Unity.DataBind.Tests.Core.Foundation.Providers.Checks
{
    using NUnit.Framework;
    using Slash.Unity.DataBind.Editor.Tests.Core.Foundation;
    using Slash.Unity.DataBind.Foundation.Providers.Checks;

    public class ComparisonCheckTest
    {
        [Test]
        public void InitWithTrue()
        {
            var providerA = new TestDataProvider<int>() {TestValue = 4};
            var providerB = new TestDataProvider<int>() {TestValue = 4};

            var check = new ComparisonCheck(providerA, providerB, ComparisonType.Equal);
            Assert.IsTrue(check.Value);
        }
        [Test]
        public void InitWithFalse()
        {
            var providerA = new TestDataProvider<int>() {TestValue = 4};
            var providerB = new TestDataProvider<int>() {TestValue = 6};

            var check = new ComparisonCheck(providerA, providerB, ComparisonType.GreaterThan);
            Assert.IsFalse(check.Value);
        }
    }
}