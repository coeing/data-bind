﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SmoothCollectionChangesFormatterTest.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.Editor.Tests.Core.Foundation.Providers.Formatters
{
    using NUnit.Framework;
    using Slash.Unity.DataBind.Core.Data;
    using Slash.Unity.DataBind.Core.Presentation;
    using Slash.Unity.DataBind.Foundation.Providers.Formatters;
    using UnityEngine;

    public class SmoothCollectionChangesFormatterTest
    {
        [Test]
        public void NoDelayForFirstItem()
        {
            var testCollection = new Collection<int> {1, 2, 3, 4, 5};

            var formatter = this.CreateTestFormatter(testCollection, 10);

            // Do a short update.
            formatter.UpdateProvider(0.1f);

            var smoothedCollection = formatter.Value as Collection;
            Assert.NotNull(smoothedCollection);
            Assert.AreEqual(1, smoothedCollection.Count);
        }

        [Test]
        public void NoDelayForFirstItemAfterClear()
        {
            const int interval = 1;

            var testCollection = new Collection<int> {1, 2, 3, 4, 5};
            var formatter = this.CreateTestFormatter(testCollection, interval);

            // Do short update which adds first item to smoothed collection.
            formatter.UpdateProvider(0.01f);

            // Clear initial collection.
            testCollection.Clear();

            // Fill with new values.
            const int newValue = 10;
            testCollection.Add(newValue);

            // Do a short update.
            formatter.UpdateProvider(0.1f);

            var smoothedCollection = formatter.Value as Collection;
            Assert.NotNull(smoothedCollection);
            Assert.AreEqual(1, smoothedCollection.Count);
            Assert.AreEqual(smoothedCollection[0], newValue);
        }

        private SmoothCollectionChangesFormatter CreateTestFormatter<T>(Collection<T> testCollection, float interval)
        {
            var gameObject = new GameObject();
            var formatter = gameObject.AddComponent<SmoothCollectionChangesFormatter>();

            var testDataProvider = new TestDataProvider<Collection<T>> {TestValue = testCollection};
            formatter.Collection = new DataBinding();
            formatter.Collection.InitProvider(testDataProvider);
            formatter.Interval = interval;

            // Init formatter.
            formatter.Init();
            formatter.Enable();

            return formatter;
        }
    }
}