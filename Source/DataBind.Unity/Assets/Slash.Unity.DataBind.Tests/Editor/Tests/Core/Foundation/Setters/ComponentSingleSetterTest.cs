﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ComponentSingleSetterTest.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Slash.Unity.DataBind.Editor.Tests.Core.Foundation.Setters
{
    using System;
    using NUnit.Framework;
    using Slash.Unity.DataBind.Core.Presentation;
    using Slash.Unity.DataBind.Foundation.Setters;
    using UnityEngine;

    public class ComponentSingleSetterTest
    {
        private class TestComponent : MonoBehaviour
        {
            
        }

        private class TestComponentSingleSetter : ComponentSingleSetter<TestComponent, string>
        {
            public event Action TargetValueUpdated;

            /// <inheritdoc />
            protected override void UpdateTargetValue(TestComponent target, string value)
            {
                this.OnTargetValueUpdated();
            }

            protected virtual void OnTargetValueUpdated()
            {
                var handler = this.TargetValueUpdated;
                if (handler != null) handler();
            }
        }

        [Test]
        public void DontSetValueIfDataBindingNotInitialized()
        {
            var gameObject = new GameObject();
            var testComponent = gameObject.AddComponent<TestComponent>();

            // Add setter.
            var componentSingleSetter = gameObject.AddComponent<TestComponentSingleSetter>();
            componentSingleSetter.Data = new DataBinding() { Type = DataBindingType.Context };
            componentSingleSetter.TargetBinding =
                new DataBinding() {Type = DataBindingType.Reference, Reference = testComponent};

            bool targetValueUpdated = false;
            componentSingleSetter.TargetValueUpdated += () => targetValueUpdated = true;

            // Init setter.
            componentSingleSetter.Init();

            Assert.IsFalse(componentSingleSetter.Data.IsInitialized);

            // Enable setter.
            componentSingleSetter.Enable();

            Assert.IsFalse(targetValueUpdated);
        }
    }
}