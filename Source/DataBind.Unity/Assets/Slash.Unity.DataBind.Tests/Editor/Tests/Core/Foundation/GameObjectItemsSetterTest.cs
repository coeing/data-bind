﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GameObjectItemsSetterTest.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.Editor.Tests.Core.Foundation
{
    using NUnit.Framework;

    using Slash.Unity.DataBind.Core.Presentation;
    using Slash.Unity.DataBind.Foundation.Setters;

    using UnityEditor;

    using UnityEngine;

    public class GameObjectItemsSetterTest
    {
        /// <summary>
        ///   GameObjectItemsSetter should disable its template if it is a scene object.
        /// </summary>
        [Test]
        public void DisableScenePrefab()
        {
            var prefab = new GameObject("Prefab");

            var gameObject = new GameObject();
            var itemsSetter = gameObject.AddComponent<GameObjectItemsSetter>();
            itemsSetter.Prefab = prefab;
            itemsSetter.Data = new DataBinding { Constant = "1", Type = DataBindingType.Constant };
            itemsSetter.TargetBinding = new DataBinding { Reference = gameObject.transform, Type = DataBindingType.Reference };
            itemsSetter.Init();

            Assert.IsFalse(prefab.activeSelf, "Scene template wasn't made inactive by items setter");

            // Cleanup.
            Object.DestroyImmediate(gameObject);
            Object.DestroyImmediate(prefab);
        }

        /// <summary>
        ///   GameObjectItemsSetter shouldn't disable its template if its a prefab.
        /// </summary>
        [Test]
        public void DontDisablePrefab()
        {
            var prefabGameObject = new GameObject("Prefab");
            const string testPrefabPath = "Assets/GameObjectItemsSetterTestPrefab.prefab";
            var prefab = PrefabUtility.SaveAsPrefabAsset(prefabGameObject, testPrefabPath);

            var gameObject = new GameObject();
            var itemsSetter = gameObject.AddComponent<GameObjectItemsSetter>();
            itemsSetter.Prefab = prefab;
            itemsSetter.Data = new DataBinding { Constant = "1", Type = DataBindingType.Constant };
            itemsSetter.TargetBinding = new DataBinding { Reference = gameObject.transform, Type = DataBindingType.Reference };
            itemsSetter.Init();

            Assert.IsTrue(prefab.activeSelf, "Prefab was made inactive by items setter");

            // Cleanup.
            Object.DestroyImmediate(gameObject);
            Object.DestroyImmediate(prefabGameObject);
            AssetDatabase.DeleteAsset(testPrefabPath);
        }

        /// <summary>
        ///   GameObjectItemsSetter shouldn't disable its template if its a prefab.
        /// </summary>
        [Test]
        public void DontDisablePrefabAfterCreateItem()
        {
            var prefabGameObject = new GameObject("Prefab");
            const string testPrefabPath = "Assets/GameObjectItemsSetterTestPrefab.prefab";
            var prefab = PrefabUtility.SaveAsPrefabAsset(prefabGameObject, testPrefabPath);

            var gameObject = new GameObject();
            var itemsSetter = gameObject.AddComponent<GameObjectItemsSetter>();
            itemsSetter.Prefab = prefab;
            itemsSetter.Data = new DataBinding { Constant = "1", Type = DataBindingType.Constant };
            itemsSetter.TargetBinding = new DataBinding { Reference = gameObject.transform, Type = DataBindingType.Reference };
            itemsSetter.Init();
            itemsSetter.Enable();

            Assert.IsTrue(prefab.activeSelf, "Prefab was made inactive by items setter");

            // Cleanup.
            Object.DestroyImmediate(gameObject);
            Object.DestroyImmediate(prefabGameObject);
            AssetDatabase.DeleteAsset(testPrefabPath);
        }
    }
}