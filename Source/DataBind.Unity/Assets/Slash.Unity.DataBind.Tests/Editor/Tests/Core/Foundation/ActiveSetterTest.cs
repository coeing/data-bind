﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ActiveSetterTest.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.Editor.Tests.Core.Foundation
{
    using NUnit.Framework;
    using Slash.Unity.DataBind.Core.Data;
    using Slash.Unity.DataBind.Core.Presentation;
    using Slash.Unity.DataBind.Foundation.Setters;
    using UnityEngine;

    public class ActiveSetterTest
    {
        [Test]
        public void SetActiveOnTrue()
        {
            // Create test object.
            var gameObject = new GameObject();
            var activeSetter = gameObject.AddComponent<ActiveSetter>();
            activeSetter.Data = new DataBinding {Constant = "true", Type = DataBindingType.Constant};
            activeSetter.TargetBinding = new DataBinding {Reference = gameObject, Type = DataBindingType.Reference};
            activeSetter.Init();
            activeSetter.Enable();

            Assert.IsTrue(gameObject.activeSelf, "GameObject is not active although binding is true");

            // Cleanup.
            Object.DestroyImmediate(gameObject);
        }

        [Test]
        public void SetInactiveOnFalse()
        {
            // Create test object.
            var gameObject = new GameObject();
            var activeSetter = gameObject.AddComponent<ActiveSetter>();
            activeSetter.Data = new DataBinding {Constant = "false", Type = DataBindingType.Constant};
            activeSetter.TargetBinding = new DataBinding {Reference = gameObject, Type = DataBindingType.Reference};
            activeSetter.Init();
            activeSetter.Enable();

            Assert.IsFalse(gameObject.activeSelf, "GameObject is active although binding is false");

            // Cleanup.
            Object.DestroyImmediate(gameObject);
        }

        [Test]
        public void SwitchFromInactiveToActive()
        {
            // Create data provider.
            var testDataProvider = new TestDataProvider<bool>();

            // Create test object.
            var gameObject = new GameObject();
            var activeSetter = gameObject.AddComponent<ActiveSetter>();

            activeSetter.Data = new DataBinding {Type = DataBindingType.Provider};
            activeSetter.Data.InitProvider(testDataProvider);

            activeSetter.TargetBinding = new DataBinding {Reference = gameObject, Type = DataBindingType.Reference};
            activeSetter.Init();
            activeSetter.Enable();

            Assert.IsFalse(gameObject.activeSelf, "GameObject is active although binding is false");

            // Switch to active.
            testDataProvider.TestValue = true;

            Assert.IsTrue(gameObject.activeSelf, "GameObject is not active although binding was switched to true");

            // Cleanup.
            Object.DestroyImmediate(gameObject);
        }
    }

    public class TestDataProvider<T> : IDataProvider
    {
        private T testValue;

        /// <inheritdoc />
        public event ValueChangedDelegate ValueChanged;

        public object Value
        {
            get
            {
                return this.TestValue;
            }
        }

        /// <inheritdoc />
        public bool IsInitialized
        {
            get
            {
                return true;
            }
        }

        public T TestValue
        {
            get
            {
                return this.testValue;
            }
            set
            {
                if (Equals(value, this.testValue))
                {
                    return;
                }

                this.testValue = value;

                this.OnValueChanged();
            }
        }

        protected virtual void OnValueChanged()
        {
            var handler = this.ValueChanged;
            if (handler != null)
            {
                handler();
            }
        }
    }
}