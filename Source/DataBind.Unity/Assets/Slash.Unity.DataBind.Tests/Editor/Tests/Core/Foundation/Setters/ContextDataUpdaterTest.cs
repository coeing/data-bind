﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ContextDataUpdaterTest.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.Editor.Tests.Core.Foundation.Setters
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using NUnit.Framework;
    using Slash.Unity.DataBind.Core.Data;
    using Slash.Unity.DataBind.Core.Presentation;
    using Slash.Unity.DataBind.Foundation.Providers.Getters;
    using Slash.Unity.DataBind.Foundation.Setters;
    using UnityEngine;

    public class ContextDataUpdaterTest
    {
        /// <summary>
        ///     Tests that a two way binding will first update from context to presentation before updating the context from
        ///     presentation side.
        ///     This should work within one frame and no matter in which order the components are initialized/updated.
        /// </summary>
        [Test]
        public void TwoWayBindingTest()
        {
            this.TwoWayBindingTest((componentDataProvider, contextDataUpdater, componentDataSetter) =>
                new DataBindingOperator[] {componentDataSetter, contextDataUpdater, componentDataProvider});
            this.TwoWayBindingTest((componentDataProvider, contextDataUpdater, componentDataSetter) =>
                new DataBindingOperator[] {componentDataProvider, contextDataUpdater, componentDataSetter});
            this.TwoWayBindingTest((componentDataProvider, contextDataUpdater, componentDataSetter) =>
                new DataBindingOperator[] {componentDataSetter, componentDataProvider, contextDataUpdater});
        }

        private void SetupTestCase(out TwoWayBindingContext context, out TestComponent component,
            out TestComponentValueProvider componentDataProvider, out ContextDataUpdater contextDataUpdater,
            out TestComponentValueSetter componentDataSetter)
        {
            context = new TwoWayBindingContext {Value = true};

            var gameObject = new GameObject();
            var contextHolder = gameObject.AddComponent<ContextHolder>();
            contextHolder.Context = context;
            component = gameObject.AddComponent<TestComponent>();
            component.Value = false;

            componentDataProvider = gameObject.AddComponent<TestComponentValueProvider>();
            componentDataProvider.TargetBinding =
                new DataBinding {Reference = component, Type = DataBindingType.Reference};

            contextDataUpdater = gameObject.AddComponent<ContextDataUpdater>();
            contextDataUpdater.Data =
                new DataBinding {Reference = componentDataProvider, Type = DataBindingType.Provider};
            contextDataUpdater.Path = "Value";

            componentDataSetter = gameObject.AddComponent<TestComponentValueSetter>();
            componentDataSetter.Data = new DataBinding {Type = DataBindingType.Context, Path = "Value"};
            componentDataSetter.TargetBinding =
                new DataBinding {Type = DataBindingType.Reference, Reference = component};
        }

        private void TwoWayBindingTest(
            Func<TestComponentValueProvider, ContextDataUpdater, TestComponentValueSetter,
                IEnumerable<DataBindingOperator>> sortComponents)
        {
            TwoWayBindingContext context;
            TestComponent testComponent;
            TestComponentValueProvider componentDataProvider;
            ContextDataUpdater contextDataUpdater;
            TestComponentValueSetter componentDataSetter;
            this.SetupTestCase(out context, out testComponent, out componentDataProvider, out contextDataUpdater,
                out componentDataSetter);

            // Init components.
            var components = sortComponents(componentDataProvider, contextDataUpdater, componentDataSetter).ToList();
            foreach (var component in components)
            {
                component.Init();
            }

            foreach (var component in components)
            {
                component.Enable();
            }

            DataBindRunner.Instance.LateUpdate();

            Assert.IsTrue(context.Value);
            Assert.IsTrue(testComponent.Value);
        }

        private class TwoWayBindingContext : Context
        {
            private readonly Property<bool> valueProperty =
                new Property<bool>();

            public bool Value
            {
                get
                {
                    return this.valueProperty.Value;
                }
                set
                {
                    this.valueProperty.Value = value;
                }
            }
        }

        private class TestComponent : MonoBehaviour
        {
            public bool Value;
        }

        private class TestComponentValueSetter : ComponentSingleSetter<TestComponent, bool>
        {
            /// <inheritdoc />
            protected override void UpdateTargetValue(TestComponent target, bool value)
            {
                target.Value = value;
            }
        }

        private class TestComponentValueProvider : ComponentPullDataProvider<TestComponent, bool>
        {
            /// <inheritdoc />
            protected override bool GetValue(TestComponent target)
            {
                return target.Value;
            }
        }
    }
}