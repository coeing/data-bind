﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CollectionRangeLookupTest.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.Editor.Tests.Core.Foundation.Providers.Lookups
{
    using NUnit.Framework;
    using Slash.Unity.DataBind.Core.Data;
    using Slash.Unity.DataBind.Foundation.Providers.Lookups;

    public class CollectionRangeLookupTest
    {
        [Test]
        public void AddItemWhileRangeBiggerThanData()
        {
            var dataCollection = new Collection<int> {1, 2, 3, 4};
            var rangeLookup = new CollectionRangeProvider
            {
                DataCollection = dataCollection,
                FirstIndex = 3,
                LastIndex = 5
            };

            // Add item that should belong to result.
            dataCollection.Add(5);
            dataCollection.Add(6);
            dataCollection.Add(7);

            Assert.That(rangeLookup.Value, Is.EquivalentTo(new[] {4, 5, 6}));
        }

        [Test]
        public void ChangeFirstIndexToLower()
        {
            var dataCollection = new Collection<int> {1, 2, 3, 4, 5, 6, 7};
            var rangeLookup = new CollectionRangeProvider
            {
                DataCollection = dataCollection,
                FirstIndex = 3,
                LastIndex = 5
            };

            // Change first index to lower index.
            rangeLookup.FirstIndex = 1;

            Assert.That(rangeLookup.Value, Is.EquivalentTo(new[] {2, 3, 4, 5, 6}));
        }

        [Test]
        public void ChangeLastIndexToLower()
        {
            var dataCollection = new Collection<int> {1, 2, 3, 4, 5, 6, 7};
            var rangeLookup = new CollectionRangeProvider
            {
                DataCollection = dataCollection,
                LastIndex = 5
            };

            // Change to lower index.
            rangeLookup.LastIndex = 3;

            Assert.That(rangeLookup.Value, Is.EquivalentTo(new[] {1, 2, 3, 4}));
        }

        [Test]
        public void EmptyDataCollection()
        {
            var dataCollection = new Collection<int>();
            var rangeLookup = new CollectionRangeProvider {DataCollection = dataCollection};
            Assert.IsNotNull(rangeLookup.Value);
        }

        [Test]
        public void InsertItemWithinLookupRange()
        {
            var dataCollection = new Collection<int> {1, 2, 3, 4, 5, 6, 7};
            var rangeLookup = new CollectionRangeProvider
            {
                DataCollection = dataCollection,
                FirstIndex = 3,
                LastIndex = 5
            };

            // Insert item within range.
            dataCollection.Insert(4, 42);

            Assert.That(rangeLookup.Value, Is.EquivalentTo(new[] {4, 42, 5}));
        }

        [Test]
        public void NullDataCollection()
        {
            var rangeLookup = new CollectionRangeProvider();
            Assert.IsNull(rangeLookup.Value);
        }

        [Test]
        public void RemoveItemWhichWasInResult()
        {
            var dataCollection = new Collection<int> {1, 2, 3, 4, 5, 6, 7};
            var rangeLookup = new CollectionRangeProvider
            {
                DataCollection = dataCollection,
                FirstIndex = 3,
                LastIndex = 5
            };

            // Remove item that belongs to result.
            dataCollection.Remove(5);

            Assert.That(rangeLookup.Value, Is.EquivalentTo(new[] {4, 6, 7}));
        }

        [Test]
        public void ReplaceItemWithinLookupRange()
        {
            var dataCollection = new Collection<int> {1, 2, 3, 4, 5, 6, 7};
            var rangeLookup = new CollectionRangeProvider
            {
                DataCollection = dataCollection,
                FirstIndex = 3,
                LastIndex = 5
            };

            // Replace item within range.
            dataCollection[4] = 42;

            Assert.That(rangeLookup.Value, Is.EquivalentTo(new[] {4, 42, 6}));
        }

        [Test]
        public void SetFirstIndex()
        {
            var dataCollection = new Collection<int> {1, 2, 3, 4, 5, 6, 7};
            var rangeLookup = new CollectionRangeProvider
            {
                DataCollection = dataCollection,
                FirstIndex = 3,
                LastIndex = 5
            };
            Assert.That(rangeLookup.Value, Is.EquivalentTo(new[] {4, 5, 6}));
        }

        [Test]
        public void SetLastIndex()
        {
            var dataCollection = new Collection<int> {1, 2, 3, 4, 5, 6, 7};
            var rangeLookup = new CollectionRangeProvider
            {
                DataCollection = dataCollection,
                LastIndex = 5
            };
            Assert.That(rangeLookup.Value, Is.EquivalentTo(new[] {1, 2, 3, 4, 5, 6}));
        }
    }
}