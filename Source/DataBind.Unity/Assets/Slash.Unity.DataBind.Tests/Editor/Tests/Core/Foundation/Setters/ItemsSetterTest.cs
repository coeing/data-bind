﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ItemsSetterTest.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.Editor.Tests.Foundation.Setters
{
    using System;
    using NUnit.Framework;
    using Slash.Unity.DataBind.Core.Data;
    using Slash.Unity.DataBind.Core.Presentation;
    using Slash.Unity.DataBind.Editor.Tests.Core.Foundation;
    using Slash.Unity.DataBind.Foundation.Setters;
    using UnityEngine;

    public class ItemsSetterTest
    {
        private class TestItemsSetter : ItemsSetter
        {
            public event Action<object, int> CreateItemCalled;

            /// <inheritdoc />
            protected override void ClearItems()
            {
            }

            /// <inheritdoc />
            protected override void CreateItem(object itemContext, int itemIndex)
            {
                var handler = this.CreateItemCalled;
                if (handler != null)
                {
                    handler(itemContext, itemIndex);
                }
            }

            /// <inheritdoc />
            protected override void RemoveItem(object itemContext)
            {
            }
        }

        [Test]
        public void Initialization_SendsCorrectItemIndexToCreateItem()
        {
            var collection = new Collection<string> {"First Item"};
            var component = CreateTestComponent(collection);

            var callIndex = -1;
            component.CreateItemCalled += (item, index) =>
            {
                callIndex = index;
            };

            // Enable component
            component.Enable();

            Assert.AreEqual(0, callIndex);
        }

        [Test]
        public void AddItem_SendsCorrectItemIndexToCreateItem()
        {
            var collection = new Collection<string>();
            var component = CreateTestComponent(collection);

            var callIndex = -1;
            component.CreateItemCalled += (item, index) =>
            {
                callIndex = index;
            };

            // Enable component
            component.Enable();

            // Add item to collection
            collection.Add("First Item");

            Assert.AreEqual(0, callIndex);
        }

        private static TestItemsSetter CreateTestComponent(Collection collection)
        {
            var gameObject = new GameObject();

            var component = gameObject.AddComponent<TestItemsSetter>();

            component.TargetBinding = new DataBinding();
            var targetProvider = new TestDataProvider<Transform>() { TestValue = gameObject.transform };
            component.TargetBinding.InitProvider(targetProvider);

            component.Data = new DataBinding();
            var dataProvider = new TestDataProvider<Collection> { TestValue = collection };
            component.Data.InitProvider(dataProvider);

            return component;
        }
    }
}