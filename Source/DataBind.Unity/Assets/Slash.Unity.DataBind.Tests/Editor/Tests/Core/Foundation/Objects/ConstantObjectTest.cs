﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ConstantObjectTest.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Slash.Unity.DataBind.Editor.Tests.Core.Foundation.Objects
{
    using NUnit.Framework;
    using Slash.Unity.DataBind.Foundation.Providers.Objects;
    using UnityEngine;

    public class ConstantObjectTest
    {
        private class TestConstantObject : ConstantObjectProvider<string>
        {
#pragma warning disable 649
            private string constantValue;
#pragma warning restore 649

            /// <inheritdoc />
            public override string ConstantValue
            {
                get
                {
                    return this.constantValue;
                }
            }
        }

        [Test]
        public void CallValueChangedOnce()
        {
            var gameObject = new GameObject();
            var constantObject = gameObject.AddComponent<TestConstantObject>();
            var valueChangedCalled = 0;
            constantObject.ValueChanged += () => valueChangedCalled++;
            constantObject.Init();
            constantObject.Enable();

            Assert.AreEqual(1, valueChangedCalled);
        }
    }
}