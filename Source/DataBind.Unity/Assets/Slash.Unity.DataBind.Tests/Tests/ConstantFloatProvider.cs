﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IntProvider.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.Tests
{
    using Slash.Unity.DataBind.Foundation.Providers.Objects;
    using UnityEngine;

    public class ConstantFloatProvider : ConstantObjectProvider<float>
    {
        [SerializeField]
        protected float constantValue;

        /// <inheritdoc />
        public override float ConstantValue
        {
            get
            {
                return this.constantValue;
            }
        }
    }
}