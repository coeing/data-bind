﻿using Slash.Unity.DataBind.Core.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Tests
{
    public class ToggleContext : Context
    {
        private readonly Property<bool> isOnProperty = new Property<bool>();

        public string Name { get; set; }

        public bool IsOn
        {
            get
            {
                return this.isOnProperty.Value;
            }
            set
            {
                if (value == this.isOnProperty.Value)
                {
                    return;
                }
                Debug.LogFormat("Change IsOn of '{0}' to '{1}'", this.Name, value);
                this.isOnProperty.Value = value;
            }
        }


    }
}
