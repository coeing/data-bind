﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Slash.Unity.DataBind.Core.Presentation;
using Tests;

public class TestContextHolder : MonoBehaviour {


    public ContextHolder contextHolder;

    ToggleContext toggleContext1;
    ToggleContext toggleContext2;

    protected virtual void Awake()
    {
        toggleContext1 = new ToggleContext()
        {
            Name = "1",
            IsOn = true
        };

        toggleContext2 = new ToggleContext()
        {
            Name = "2",
            IsOn = false
        };

        contextHolder.Context = toggleContext1;

    }


    // Use this for initialization
    void Start () {

    }

    public void SwitchContext()
    {
        contextHolder.Context = toggleContext2;
        contextHolder.Context = toggleContext1;
    }

    // Update is called once per frame
    void Update () {
        
    }
}
