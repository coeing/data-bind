﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TestDataBindingOperatorOverwriteInit.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.Tests
{
    using Slash.Unity.DataBind.Core.Presentation;
    using UnityEngine;

    public class TestDataBindingOperatorOverwriteInit : DataBindingOperator
    {
        /// <inheritdoc />
        public override void Init()
        {
            Debug.Log("Init in derived DataBindingOperator");
        }
    }
}