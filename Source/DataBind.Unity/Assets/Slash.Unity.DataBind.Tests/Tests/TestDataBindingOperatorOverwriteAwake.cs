﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TestDataBindingOperatorOverwriteAwake.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.Tests
{
    using Slash.Unity.DataBind.Core.Presentation;
    using UnityEngine;

    public class TestDataBindingOperatorOverwriteAwake : DataBindingOperator
    {
        protected new void Awake()
        {
            Debug.Log("Awake in derived DataBindingOperator");
        }
    }
}