﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ConstantIntProvider.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.Tests
{
    using Slash.Unity.DataBind.Foundation.Providers.Objects;
    using UnityEngine;

    public class ConstantIntProvider : ConstantObjectProvider<int>
    {
        [SerializeField]
        protected int constantValue;

        /// <inheritdoc />
        public override int ConstantValue
        {
            get
            {
                return this.constantValue;
            }
        }
    }
}