﻿namespace Slash.Unity.DataBind.Tests.ContextHolderContextChange
{
    using Slash.Unity.DataBind.Core.Data;
    using UnityEngine;

    public class TestContext : Context
    {
        private readonly Property<string> testNameProperty = new Property<string>();

        private readonly Property<float> testValueProperty = new Property<float>();

        public string TestName
        {
            get
            {
                return this.testNameProperty.Value;
            }
            set
            {
                this.testNameProperty.Value = value;
            }
        }

        public float TestValue
        {
            get
            {
                return this.testValueProperty.Value;
            }
            set
            {
                this.testValueProperty.Value = value;
                Debug.Log("Testvalue set to " + value + " on " + this.TestName);
            }
        }
    }
}