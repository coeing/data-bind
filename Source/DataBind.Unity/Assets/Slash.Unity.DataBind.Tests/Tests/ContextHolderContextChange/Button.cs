﻿namespace Slash.Unity.DataBind.Tests.ContextHolderContextChange
{
    using Slash.Unity.DataBind.Core.Presentation;
    using UnityEngine;

    public class Button : MonoBehaviour
    {
        public string AssignName = "";

        public ContextHolder MainContextHolder;

        private TestContext myValues;

        public void Start()
        {
            this.myValues = new TestContext {TestName = this.AssignName};
            this.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(this.SetContext);
        }

        private void SetContext()
        {
            this.MainContextHolder.Context = this.myValues;
        }
    }
}