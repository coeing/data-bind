﻿<?xml version = "1.0" encoding = "utf-8"?>

<topic id = "ba8b60bd-4f7c-414f-9c06-1afc38627fb6" revisionNumber = "1">
  <developerConceptualDocument
    xmlns = "http://ddue.schemas.microsoft.com/authoring/2003/5"
    xmlns:xlink = "http://www.w3.org/1999/xlink">
    <introduction>
      <para>Version 1.0.2 was released on 28/02/2015.</para>
    </introduction>

    <section>
      <title>Changes in This Release</title>
      <content>

        <list class = "bullet">
          <listItem>
            <para>ADDED: Core - Added exceptions if trying to set a read-only property or method.</para>
          </listItem>
          <listItem>
            <para>
              CHANGED: Core - DataNode sets value of field or property instead of data property, so even raw
              members of a context are updated correctly.
            </para>
          </listItem>
          <listItem>
            <para>
              CHANGED: Bindings - Renamed GameObjectSetter to GameObjectSingleSetter.
            </para>
          </listItem>
          <listItem>
            <para>
              CHANGED: Core - Made some methods of context holder virtual to allow deriving from the class if
              necessary.
            </para>
          </listItem>
          <listItem>
            <para>FIXED: Data Bind - Returning path to context if no path is set after #X notation.</para>
          </listItem>
          <listItem>
            <para>
              CHANGED: Data Bind - Logging error instead of exception if invalid path was specified (on iOS
              exceptions caused a crash).
            </para>
          </listItem>
          <listItem>
            <para>
              CHANGED: Editor - Adjusted component menu titles for bindings to be more consistent.
            </para>
          </listItem>
          <listItem>
            <para>
              ADDED: Setters - Added binding to create a child game object from a prefab with a specific context.
            </para>
          </listItem>
          <listItem>
            <para>
              ADDED: GraphicColorSetter - Setter for color of a Graphic component (e.g. Text and Image).
            </para>
          </listItem>
          <listItem>
            <para>
              CHANGED: ArithmeticOperation - Checking second argument for zero before doing division to avoid
              exception.
            </para>
          </listItem>
          <listItem>
            <para>
              ADDED: Unity UI - Added SelectableInteractableSetter to change if a selectable is interactable depending
              on a boolean data value.
            </para>
          </listItem>
          <listItem>
            <para>
              ADDED: Operations - Added sprite loader which loads a sprite depending on a string value.
            </para>
          </listItem>
          <listItem>
            <para>
              ADDED: Core - Added log error when context is not derived from Context class, but path is set. This may
              indicate that the user forgot to derive from Context class.
            </para>
          </listItem>
          <listItem>
            <para>
              ADDED: Context Path - Added property drawer for a context path property.
            </para>
          </listItem>
          <listItem>
            <para>
              CHANGED: Context Holder - Only creating context automatically if explicitly stated and no context is
              already set in Awake.
            </para>
          </listItem>
        </list>
      </content>
    </section>

    <relatedTopics>
      <link xlink:href = "a5e9ddc1-2baf-4129-8bd4-70c0ee2146c8" />
    </relatedTopics>

  </developerConceptualDocument>
</topic>