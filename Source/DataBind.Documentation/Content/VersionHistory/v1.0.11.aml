﻿<?xml version="1.0" encoding="utf-8"?>
<topic id="8754002d-e852-42d7-aa6f-74ca00f54408" revisionNumber="1">
  <developerConceptualDocument
    xmlns="http://ddue.schemas.microsoft.com/authoring/2003/5"
    xmlns:xlink="http://www.w3.org/1999/xlink">
 <introduction>
      <para>Version 1.0.11 was released on 12/11/2017.</para>
    </introduction>
    <section>
      <title>Changes in This Release</title>
      <content>
        <list class="bullet">
          <listItem>
            <para>Call first Enable of a data binding operator in the Start method instead of OnEnable to make sure that all other scripts had time to initialize in their Awake method (Unity may call OnEnable of an object before the Awake method of other objects was called, see https://forum.unity.com/threads/onenable-before-awake.361429/)</para>
          </listItem>
          <listItem>
            <para>Add small SpriteLoading example that also shows the issue #59 (invalid warning from SpriteLoader)</para>
          </listItem>
          <listItem>
            <para>Issue #58 Add indexer to Collection class</para>
          </listItem>
          <listItem>
            <para>Issue #63 Add warning if more than one settings objects are found and none is called like the default one</para>
          </listItem>
          <listItem>
            <para>Issue #63 Use any DataBindSettings object found in resources when not found at default path</para>
          </listItem>
          <listItem>
            <para>Issue #63 Made DataBindSettings editable again</para>
          </listItem>
          <listItem>
            <para>Issue #62 Show items of any IEnumerable in ContextHolderEditor and add buttons to remove/add for ICollection members (which includes Collection class)</para>
          </listItem>
          <listItem>
            <para>Issue #34 Add error when target binding doesn't receive an object of the expected type </para>
          </listItem>
          <listItem>
            <para>Use GameObjectItemsSetter instead of LayoutGroupItemsSetter and flag LayoutGroupItemsSetter as obsolete</para>
          </listItem>
          <listItem>
            <para>Use virtual method NotifyContextOperatorsAboutContextChange instead of IContextHolder interface</para>
          </listItem>
          <listItem>
            <para>Only update context data in ContextDataUpdater if active and enabled</para>
          </listItem>
          <listItem>
            <para>Only use Coroutine for delayed value initialization in DataContextNodeConnector if mono behaviour is enabled</para>
          </listItem>
          <listItem>
            <para>Issue #60 Only register listener when a valueChangedCallback is set, otherwise an initial value 'null' is returned which changes the context value incorrectly</para>
          </listItem>
          <listItem>
            <para>Issue #60 Add example that shows a bug when switching the context of a context holder</para>
          </listItem>
          <listItem>
            <para>Issue #49 Delay data update when context changed till end of frame as there may be multiple data context node connectors that have to update their context first</para>
          </listItem>
          <listItem>
            <para>Use MonoBehaviour a data binding and data context node connector belong to instead of game object</para>
          </listItem>
          <listItem>
            <para>Add test case for issue #49</para>
          </listItem>
          <listItem>
            <para>Issue #45 Make GetValue and SetValue of a context work with struct members</para>
          </listItem>
          <listItem>
            <para>Issue #56 Make Unity callbacks protected in DataBindingOperator, so there is at least a warning when overriding them instead of the virtual methods (A derived class should use the virtual methods (Init, Deinit, Enable, Disable) instead of the Unity callbacks, so the initialization order remains correct.)</para>
          </listItem> 
        </list>
      </content>
    </section>
    <relatedTopics>
      <link xlink:href="a5e9ddc1-2baf-4129-8bd4-70c0ee2146c8" />
    </relatedTopics>
  </developerConceptualDocument>
</topic>
